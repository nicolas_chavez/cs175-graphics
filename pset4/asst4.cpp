
////////////////////////////////////////////////////////////////////////
//
//   Harvard University
//   CS175 : Computer Graphics
//   Professor Steven Gortler
//
////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <memory>
#include <stdexcept>
#if __GNUG__
#   include <tr1/memory>
#endif

#ifdef __MAC__
#   include <OpenGL/gl3.h>
#   include <GLUT/glut.h>
#else
#   include <GL/glew.h>
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "geometrymaker.h"
#include "ppm.h"
#include "glsupport.h"
#include "rigtform.h"
#include "arcball.h"
#include "asstcommon.h"
#include "scenegraph.h"
#include "drawer.h"
#include "picker.h"

using namespace std;  // for string, vector, iostream, and other standard C++ stuff
using namespace tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

static const float g_frustMinFov = 60.0;  // A minimal of 60 degree field of view
static float g_frustFovY = g_frustMinFov; // FOV in y direction (updated by updateFrustFovY)

static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 10.0;   // half the ground length

static int g_windowWidth = 512;
static int g_windowHeight = 512;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static bool g_spaceDown = false;         // space state, for middle mouse emulation
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event
static int g_activeShader = 0;
static int g_skyModMode = 0; //0 means world sky frame, 1 means sky sky frame

static double g_arcballScreenRadius = 0.25 * 512;
static double g_arcballScale = 1.0;
static bool g_arcballVisible = true;
static bool g_aboutToPick = false;

static const int PICKING_SHADER = 2; // index of the picking shader is g_shaerFiles
static const int g_numShaders = 3; // 3 shaders instead of 2
static const char * const g_shaderFiles[g_numShaders][2] = {
  {"./shaders/basic-gl3.vshader", "./shaders/diffuse-gl3.fshader"},
  {"./shaders/basic-gl3.vshader", "./shaders/solid-gl3.fshader"},
  {"./shaders/basic-gl3.vshader", "./shaders/pick-gl3.fshader"}
};
static const char * const g_shaderFilesGl2[g_numShaders][2] = {
  {"./shaders/basic-gl2.vshader", "./shaders/diffuse-gl2.fshader"},
  {"./shaders/basic-gl2.vshader", "./shaders/solid-gl2.fshader"},
  {"./shaders/basic-gl2.vshader", "./shaders/pick-gl2.fshader"}
};
static vector<shared_ptr<ShaderState> > g_shaderStates; // our global shader states

// --------- Geometry

// Macro used to obtain relative offset of a field within a struct
#define FIELD_OFFSET(StructType, field) &(((StructType *)0)->field)

// A vertex with floating point position and normal
struct VertexPN {
  Cvec3f p, n;

  VertexPN() {}
  VertexPN(float x, float y, float z,
           float nx, float ny, float nz)
    : p(x,y,z), n(nx, ny, nz)
  {}

  // Define copy constructor and assignment operator from GenericVertex so we can
  // use make* functions from geometrymaker.h
  VertexPN(const GenericVertex& v) {
    *this = v;
  }

  VertexPN& operator = (const GenericVertex& v) {
    p = v.pos;
    n = v.normal;
    return *this;
  }
};

struct Geometry {
  GlBufferObject vbo, ibo;
  GlArrayObject vao;
  int vboLen, iboLen;

  Geometry(VertexPN *vtx, unsigned short *idx, int vboLen, int iboLen) {
    this->vboLen = vboLen;
    this->iboLen = iboLen;
  
    // Now create the VBO and IBO
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPN) * vboLen, vtx, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * iboLen, idx, GL_STATIC_DRAW);
  }

  void draw(const ShaderState& curSS) {
  // bind the object's VAO
  glBindVertexArray(vao);
  
    // Enable the attributes used by our shader
    safe_glEnableVertexAttribArray(curSS.h_aPosition);
    safe_glEnableVertexAttribArray(curSS.h_aNormal);

    // bind vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    safe_glVertexAttribPointer(curSS.h_aPosition, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, p));
    safe_glVertexAttribPointer(curSS.h_aNormal, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, n));

    // bind ibo
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    // draw!
    glDrawElements(GL_TRIANGLES, iboLen, GL_UNSIGNED_SHORT, 0);

    // Disable the attributes used by our shader
    safe_glDisableVertexAttribArray(curSS.h_aPosition);
    safe_glDisableVertexAttribArray(curSS.h_aNormal);
  
  // disable VAO
  glBindVertexArray(NULL);
  }
};

typedef SgGeometryShapeNode<Geometry> MyShapeNode;

// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground;
static shared_ptr<Geometry> g_cube;
static shared_ptr<Geometry> g_sphere;

static shared_ptr<SgRootNode> g_world;
static shared_ptr<SgRbtNode> g_skyNode, g_groundNode, g_robot1Node, g_robot2Node;
static shared_ptr<SgRbtNode> g_currentPickedRbtNode = g_skyNode; // used later when you do picking
static shared_ptr<SgRbtNode> g_eyeFrameNode = g_skyNode;

// --------- Scene
static const Cvec3
  g_light1(2.0, 3.0, 14.0),
  g_light2(-2, -3.0, -5.0);  // define two lights positions in world space

static RigTForm g_aFrame = RigTForm();

///////////////// END OF G L O B A L S //////////////////////////////////////////////////

static void initGround() {
  // A x-z plane at y = g_groundY of dimension [-g_groundSize, g_groundSize]^2
  VertexPN vtx[4] = {
    VertexPN(-g_groundSize, g_groundY, -g_groundSize, 0, 1, 0),
    VertexPN(-g_groundSize, g_groundY,  g_groundSize, 0, 1, 0),
    VertexPN( g_groundSize, g_groundY,  g_groundSize, 0, 1, 0),
    VertexPN( g_groundSize, g_groundY, -g_groundSize, 0, 1, 0),
  };
  unsigned short idx[] = {0, 1, 2, 0, 2, 3};
  g_ground.reset(new Geometry(&vtx[0], &idx[0], 4, 6));
}

static void initCube() {

  int ibLen, vbLen;
  getCubeVbIbLen(vbLen, ibLen);

  // Temporary storage for cube geometry
  vector<VertexPN> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeCube(1, vtx.begin(), idx.begin());

  g_cube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}

static void initSphere() {

  int ibLen, vbLen;
  int slices = 20, stacks = 20;
  //getCubeVbIbLen(vbLen, ibLen);
  getSphereVbIbLen(slices, stacks, vbLen, ibLen);

  // Temporary storage for sphere geometry
  vector<VertexPN> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeSphere(1.0, slices, stacks, vtx.begin(), idx.begin());
  
  g_sphere.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}

// takes a projection matrix and send to the the shaders
static void sendProjectionMatrix(const ShaderState& curSS, const Matrix4& projMatrix) {
  GLfloat glmatrix[16];
  projMatrix.writeToColumnMajorMatrix(glmatrix); // send projection matrix
  safe_glUniformMatrix4fv(curSS.h_uProjMatrix, glmatrix);
}


// update g_frustFovY from g_frustMinFov, g_windowWidth, and g_windowHeight
static void updateFrustFovY() {
  if (g_windowWidth >= g_windowHeight)
    g_frustFovY = g_frustMinFov;
  else {
    const double RAD_PER_DEG = 0.5 * CS175_PI/180;
    g_frustFovY = atan2(sin(g_frustMinFov * RAD_PER_DEG) * g_windowHeight / g_windowWidth, cos(g_frustMinFov * RAD_PER_DEG)) / RAD_PER_DEG;
  }
}

static Matrix4 makeProjectionMatrix() {
  return Matrix4::makeProjection(
           g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
           g_frustNear, g_frustFar);
}

static void buildAFrame(){

  if (g_eyeFrameNode == g_skyNode && g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0) {
    Quat rot = getPathAccumRbt(g_world, g_currentPickedRbtNode).getRotation();
    g_aFrame = RigTForm(Cvec3(), rot);

  } else {
    Cvec3 trans = getPathAccumRbt(g_world, g_currentPickedRbtNode).getTranslation();
    Quat rot = getPathAccumRbt(g_world, g_eyeFrameNode).getRotation();
    g_aFrame = RigTForm(trans, rot);
  }
}

static RigTForm makeArcballRbt() {

  RigTForm arcballRbt;
  if (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0) {
    arcballRbt = getPathAccumRbt(g_world, g_world);
  } else {
    arcballRbt = getPathAccumRbt(g_world, g_currentPickedRbtNode);
  }

  g_arcballVisible = (g_currentPickedRbtNode != g_eyeFrameNode || (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0));
  return arcballRbt;
}

static void drawStuff(const ShaderState& curSS, bool picking) {

  // build & send proj. matrix to vshader
  const Matrix4 projmat = makeProjectionMatrix();
  sendProjectionMatrix(curSS, projmat);

  // choose frame to use as eyeRbt
  RigTForm eyeRbt = getPathAccumRbt(g_world, g_eyeFrameNode);
  
  const RigTForm invEyeRbt = inv(eyeRbt);

  const Cvec3 eyeLight1 = Cvec3(invEyeRbt * Cvec4(g_light1, 1)); // g_light1 position in eye coordinates
  const Cvec3 eyeLight2 = Cvec3(invEyeRbt * Cvec4(g_light2, 1)); // g_light2 position in eye coordinates
  safe_glUniform3f(curSS.h_uLight, eyeLight1[0], eyeLight1[1], eyeLight1[2]);
  safe_glUniform3f(curSS.h_uLight2, eyeLight2[0], eyeLight2[1], eyeLight2[2]);

  RigTForm arcballRbt = makeArcballRbt();

  if(!(g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton) || (g_mouseLClickButton && !g_mouseRClickButton && g_spaceDown))){

    // update arcballScale
    g_arcballScale = getScreenToEyeScale((invEyeRbt * arcballRbt).getTranslation()[2], g_frustFovY, g_windowHeight);
  }

  if (!picking) {

    Drawer drawer(invEyeRbt, curSS);
    g_world->accept(drawer);

    // draw arcball as part of asst3
    if (g_arcballVisible) {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // draw wireframe
  
      double s = g_arcballScale * g_arcballScreenRadius;

      RigTForm MVMrbt = invEyeRbt * arcballRbt;
      Matrix4 MVM = rigTFormToMatrix(MVMrbt);
      MVM = MVM * Matrix4::makeScale(Cvec3(s, s, s));
  
      Matrix4 NMVM = normalMatrix(MVM);
      sendModelViewNormalMatrix(curSS, MVM, NMVM);
      //purp
      safe_glUniform3f(curSS.h_uColor, 0.9, 0.1, 0.9);
      g_sphere->draw(curSS);
  
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    

  } else {//if indeed picking

    Picker picker(invEyeRbt, curSS);
    g_world->accept(picker);
    glFlush();

    g_currentPickedRbtNode = picker.getRbtNodeAtXY(g_mouseClickX, g_mouseClickY);

    if (!g_currentPickedRbtNode || g_currentPickedRbtNode == g_groundNode)
      g_currentPickedRbtNode = g_eyeFrameNode;

    g_arcballVisible = (g_currentPickedRbtNode != g_eyeFrameNode || (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0));
    g_arcballScale = getScreenToEyeScale((invEyeRbt * arcballRbt).getTranslation()[2], g_frustFovY, g_windowHeight);
  } 
}


static void pick() {

  g_aboutToPick = false;
  // We need to set the clear color to black, for pick rendering.
  // so let's save the clear color
  GLdouble clearColor[4];
  glGetDoublev(GL_COLOR_CLEAR_VALUE, clearColor);

  glClearColor(0, 0, 0, 0);

  // using PICKING_SHADER as the shader
  glUseProgram(g_shaderStates[PICKING_SHADER]->program);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  drawStuff(*g_shaderStates[PICKING_SHADER], true);

  glutPostRedisplay();

  // Uncomment below and comment out the glutPostRedisplay in mouse(...) call back
  // to see result of the pick rendering pass
  //glutSwapBuffers();

  //Now set back the clear color
  glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);

  checkGlErrors();
}

static void display() {
  glUseProgram(g_shaderStates[g_activeShader]->program);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                   // clear framebuffer color&depth

  drawStuff(*g_shaderStates[g_activeShader], false);

  glutSwapBuffers();                                    // show the back buffer (where we rendered stuff)

  checkGlErrors();
}

static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  g_arcballScreenRadius = 0.25 * min(g_windowHeight, g_windowWidth);
  glViewport(0, 0, w, h);
  cerr << "Size of window is now " << w << "x" << h << endl;
  updateFrustFovY();
  glutPostRedisplay();
}

static Quat arcballRotation(const int x, const int y){

  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  RigTForm eyeRbt = getPathAccumRbt(g_world, g_eyeFrameNode);
  RigTForm arcballRbt = makeArcballRbt();

  RigTForm invEyeRbt = inv(eyeRbt);
  Cvec2 arcballScreenCenter = getScreenSpaceCoord((invEyeRbt * arcballRbt).getTranslation(), makeProjectionMatrix(), g_frustNear, g_frustFovY, g_windowWidth, g_windowHeight);

  double x1 = g_mouseClickX - arcballScreenCenter[0];
  double y1 = g_mouseClickY - arcballScreenCenter[1];
  double z1;
  Cvec3 v1;

  double x2 = x - arcballScreenCenter[0];
  double y2 = g_windowHeight - y - 1 - arcballScreenCenter[1];
  double z2;
  Cvec3 v2;

  if (pow(x1, 2) + pow(y1, 2) > pow(g_arcballScreenRadius, 2)) {
    z1 = 0;
    v1 = normalize(Cvec3(x1, y1, z1)) * g_arcballScreenRadius;
  } else {
      z1 = sqrt(pow(g_arcballScreenRadius, 2) - pow(x1, 2) - pow(y1, 2));
      v1 = Cvec3(x1, y1, z1);
  }
  if (pow(x2, 2) + pow(y2, 2) > pow(g_arcballScreenRadius, 2)) {
    z2 = 0;
    v2 = normalize(Cvec3(x2, y2, z2)) * g_arcballScreenRadius;
  } else {
      z2 = sqrt(pow(g_arcballScreenRadius, 2) - pow(x2, 2) - pow(y2, 2));
      v2 = Cvec3(x2, y2, z2);
  }

  v1 = normalize(v1);
  v2 = normalize(v2);

  //Quat rot = Quat(0, v2) * Quat(0, -v1);
  Quat rot = Quat(dot(v1, v2), cross(v1, v2));

  return rot;
}

static void motion(const int x, const int y) {
  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  Matrix4 m;
  RigTForm mRbt;
  

  if (g_mouseLClickButton && !g_mouseRClickButton && !g_spaceDown) { // left button down?

    if (g_arcballVisible){
      mRbt = RigTForm(arcballRotation(x, y));
    } else {
      mRbt = RigTForm(Quat::makeXRotation(-dy) * Quat::makeYRotation(dx));
    }
  }
  else if (g_mouseRClickButton && !g_mouseLClickButton) { // right button down?

    if (g_arcballVisible) {
      mRbt = RigTForm(Cvec3(dx, dy, 0) * g_arcballScale);
    } else {
      mRbt = RigTForm(Cvec3(dx, dy, 0) * 0.01);
    }
    
  }
  else if (g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton) || (g_mouseLClickButton && !g_mouseRClickButton && g_spaceDown)) {  // middle or (left and right, or left + space) button down?

    if (g_arcballVisible) {
      mRbt = RigTForm(Cvec3(0, 0, -dy) * g_arcballScale);
    } else {
      mRbt = RigTForm(Cvec3(0, 0, -dy) * 0.01);
    }
    
  }

  buildAFrame();

  if (g_mouseClickDown) {
    
    RigTForm curRbt = g_currentPickedRbtNode->getRbt();

    if(g_currentPickedRbtNode == g_skyNode) {

      if (g_skyModMode == 0) {
          Quat rot = mRbt.getRotation();
          mRbt = RigTForm(-(mRbt.getTranslation()), Quat(rot[0], -(rot[1]), -(rot[2]), -(rot[3])));
          g_skyNode->setRbt(g_aFrame * mRbt * inv(g_aFrame) * curRbt);
      } else {
          Quat rot = mRbt.getRotation();
          mRbt = RigTForm(mRbt.getTranslation(), Quat(rot[0], -(rot[1]), -(rot[2]), -(rot[3])));
          g_skyNode->setRbt(g_aFrame * mRbt * inv(g_aFrame) * curRbt);
      }
    }else {
      
      //g_arcballRbt = g_aFrame * mRbt * inv(g_aFrame) * g_arcballRbt;
      RigTForm newAFrame = inv(getPathAccumRbt(g_world, g_currentPickedRbtNode, 1)) * g_aFrame;
      g_currentPickedRbtNode->setRbt(newAFrame * mRbt * inv(newAFrame) * curRbt);
    } 
    glutPostRedisplay(); // we always redraw if we changed the scene
  }

  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;
}

static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;

  ///

  if(g_mouseLClickButton){

    if(g_aboutToPick){
      pick();
    }
  }
}

static void keyboardUp(const unsigned char key, const int x, const int y) {
  switch (key) {
  case ' ':
    g_spaceDown = false;
    break;
  }
  glutPostRedisplay();
}

static void keyboard(const unsigned char key, const int x, const int y) {
  switch (key) {
  case 27:
    exit(0);                                  // ESC
  case 'h':
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "s\t\tsave screenshot\n"
    << "f\t\tToggle flat shading on/off.\n"
    << "o\t\tCycle object to edit\n"
    << "v\t\tCycle view\n"
    << "drag left mouse to rotate\n" << endl;
    break;
  case 's':
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    break;
  case 'f':
    g_activeShader ^= 1;
    break;

  case 'p':
    g_aboutToPick = true;
    break;
  case 'v':    
    if(g_eyeFrameNode == g_skyNode){

      g_eyeFrameNode = g_robot1Node;
      g_currentPickedRbtNode = g_robot1Node;
      makeArcballRbt();
      cout << "current view: robot 1" << endl;
    }
    else if (g_eyeFrameNode == g_robot1Node) {
      g_eyeFrameNode = g_robot2Node;
      g_currentPickedRbtNode = g_robot2Node;
      makeArcballRbt();
      cout << "current view: robot 2" << endl; 
    }
    else if (g_eyeFrameNode == g_robot2Node) {
      g_eyeFrameNode = g_skyNode;
      makeArcballRbt();
      cout << "current view: sky camera" << endl; 
    }
    //resetArcball();
    break;
  case 'm':
      g_skyModMode = (g_skyModMode + 1) % 2;
      switch(g_skyModMode){
        case 0:
          cout << "sky-world" << endl;
          break;
        case 1:
          cout << "sky-sky" << endl;
          break;
      }
    //resetArcball();
    break;
  case ' ':
    g_spaceDown = true;
    break;
  }
  glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
#ifdef __MAC__
  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE|GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH); // core profile flag is required for GL 3.2 on Mac
#else
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
#endif
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("Assignment 2");                       // title the window

  glutIgnoreKeyRepeat(true);                              // avoids repeated keyboard calls when holding space to emulate middle mouse

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutMotionFunc(motion);                                 // mouse movement callback
  glutMouseFunc(mouse);                                   // mouse click callback
  glutKeyboardFunc(keyboard);
  glutKeyboardUpFunc(keyboardUp);
}

static void initGLState() {
  glClearColor(128./255., 200./255., 255./255., 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initShaders() {
  g_shaderStates.resize(g_numShaders);
  for (int i = 0; i < g_numShaders; ++i) {
    if (g_Gl2Compatible)
      g_shaderStates[i].reset(new ShaderState(g_shaderFilesGl2[i][0], g_shaderFilesGl2[i][1]));
    else
      g_shaderStates[i].reset(new ShaderState(g_shaderFiles[i][0], g_shaderFiles[i][1]));
  }
}

static void initGeometry() {
  initGround();
  initCube();
  initSphere();
}

static void constructRobot(shared_ptr<SgTransformNode> base, const Cvec3& color) {

  const double ARM_LEN = 0.7,
               ARM_THICK = 0.25,
               TORSO_LEN = 1.5,
               TORSO_THICK = 0.25,
               TORSO_WIDTH = 1,
               HEAD_RADIUS = 0.4;
  const int NUM_JOINTS = 10,
            NUM_SHAPES = 10;

  struct JointDesc {
    int parent;
    float x, y, z;
  };

  JointDesc jointDesc[NUM_JOINTS] = {
    {-1, 0, 0, 0}, // torso
    {0, +TORSO_WIDTH/2, TORSO_LEN/2, 0}, // upper right arm
    {1, +ARM_LEN, 0, 0}, // lower right arm
    {0, -TORSO_WIDTH/2, TORSO_LEN/2, 0}, //upper left arm
    {3, -ARM_LEN, 0, 0}, //lower left arm

    {0, +TORSO_WIDTH*0.4, -TORSO_LEN/2, 0}, //upper right leg
    {5, 0, -ARM_LEN, 0}, //lower right leg
    {0, -TORSO_WIDTH*0.4, -TORSO_LEN/2, 0}, // upper left leg
    {7, 0, -ARM_LEN, 0}, //lower left leg

    {0, 0, +TORSO_LEN/2, 0}, //head
  };

  struct ShapeDesc {
    int parentJointId;
    float x, y, z, sx, sy, sz;
    shared_ptr<Geometry> geometry;
  };

  ShapeDesc shapeDesc[NUM_SHAPES] = {
    {0, 0,         0, 0, TORSO_WIDTH, TORSO_LEN, TORSO_THICK, g_cube}, // torso
    {1, +ARM_LEN/2, 0, 0, ARM_LEN * 0.55, ARM_THICK * 0.6, ARM_THICK * 0.6, g_sphere}, // upper right arm
    {2, +ARM_LEN/2, 0, 0, ARM_LEN, ARM_THICK, ARM_THICK, g_cube}, // lower right arm
    {3, -ARM_LEN/2, 0, 0, ARM_LEN * 0.55, ARM_THICK * 0.6, ARM_THICK * 0.6, g_sphere}, //upper left arm
    {4, -ARM_LEN/2, 0, 0, ARM_LEN, ARM_THICK, ARM_THICK, g_cube}, //lower left arm

    {5, 0, -ARM_LEN/2, 0, ARM_THICK * 0.6, ARM_LEN * 0.55, ARM_THICK * 0.6, g_sphere}, //upper right leg
    {6, 0, -ARM_LEN/2, 0, ARM_THICK, ARM_LEN, ARM_THICK, g_cube}, //lower right leg
    {7, 0, -ARM_LEN/2, 0, ARM_THICK * 0.6, ARM_LEN * 0.55, ARM_THICK * 0.6, g_sphere}, //upper left leg
    {8, 0, -ARM_LEN/2, 0, ARM_THICK, ARM_LEN, ARM_THICK, g_cube}, //lower left leg

    {9, 0, HEAD_RADIUS, 0, HEAD_RADIUS, HEAD_RADIUS, HEAD_RADIUS, g_sphere}, //head
  };

  shared_ptr<SgTransformNode> jointNodes[NUM_JOINTS];

  for (int i = 0; i < NUM_JOINTS; ++i) {
    if (jointDesc[i].parent == -1)
      jointNodes[i] = base;
    else {
      jointNodes[i].reset(new SgRbtNode(RigTForm(Cvec3(jointDesc[i].x, jointDesc[i].y, jointDesc[i].z))));
      jointNodes[jointDesc[i].parent]->addChild(jointNodes[i]);
    }
  }
  for (int i = 0; i < NUM_SHAPES; ++i) {
    shared_ptr<MyShapeNode> shape(
      new MyShapeNode(shapeDesc[i].geometry,
                      color,
                      Cvec3(shapeDesc[i].x, shapeDesc[i].y, shapeDesc[i].z),
                      Cvec3(0, 0, 0),
                      Cvec3(shapeDesc[i].sx, shapeDesc[i].sy, shapeDesc[i].sz)));
    jointNodes[shapeDesc[i].parentJointId]->addChild(shape);
  }
}

static void initScene() {
  g_world.reset(new SgRootNode());

  g_skyNode.reset(new SgRbtNode(RigTForm(Cvec3(0.0, 0.25, 4.0))));

  g_currentPickedRbtNode = g_skyNode;
  g_eyeFrameNode = g_skyNode;

  g_groundNode.reset(new SgRbtNode());
  g_groundNode->addChild(shared_ptr<MyShapeNode>(
                           new MyShapeNode(g_ground, Cvec3(0.1, 0.95, 0.1))));

  g_robot1Node.reset(new SgRbtNode(RigTForm(Cvec3(-2, 1, 0))));
  g_robot2Node.reset(new SgRbtNode(RigTForm(Cvec3(2, 1, 0))));

  constructRobot(g_robot1Node, Cvec3(1, 0, 0)); // a Red robot
  constructRobot(g_robot2Node, Cvec3(0, 0, 1)); // a Blue robot

  g_world->addChild(g_skyNode);
  g_world->addChild(g_groundNode);
  g_world->addChild(g_robot1Node);
  g_world->addChild(g_robot2Node);
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

  // on Mac, we shouldn't use GLEW.

#ifndef __MAC__
    glewInit(); // load the OpenGL extensions
#endif

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.5") << endl;

#ifndef __MAC__
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");
#endif

    initGLState();
    initShaders();
    initGeometry();
    initScene();

    glutMainLoop();
    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
