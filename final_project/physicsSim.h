#ifndef _PHYSICS_SIM_H__
#define _PHYSICS_SIM_H__

#include "cvec.h"
#include "rigtform.h"
#include "physicsBody.h"
#include "scenegraph.h"
#include <vector>

//simulation class
class PhysicsSim {

public:
	PhysicsSim(std::shared_ptr<SgRootNode> rootNode);
	~PhysicsSim();

	//in seconds -- udpates elements in scene
	void update(float timeElapsed);
	void add(const std::shared_ptr<HasPhysicsBody>& hpb);

	int getNumSpheres() { return sphericalBodies_.size(); }
	std::shared_ptr<HasPhysicsBody> getSphereAtIndex(int i) {
		return sphericalBodies_[i];
	}

	void setDamping(float damping) { damping_ = damping; }

protected:

	bool sphereCollision(int s1, int s2);
	void updateSphereSphereVelocities(int s1, int s2);
	void updateSpherePlaneVelocities(int s, int p);

	std::vector<std::shared_ptr<HasPhysicsBody> > sphericalBodies_;
	std::vector<std::shared_ptr<HasPhysicsBody> > planarBodies_;

	std::shared_ptr<SgRootNode> rootNode_;
	float damping_;

private:

	bool intersect_sphere_plane(std::shared_ptr<HasPhysicsBody> sphere,
		std::shared_ptr<HasPhysicsBody> plane,
		Cvec3* intersectionPoint);
};

#endif//_PHYSICS_SIM_H__