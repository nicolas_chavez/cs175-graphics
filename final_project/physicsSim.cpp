#include "physicsSim.h"

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

static bool intersect_line_plane(Cvec3 l0, Cvec3 l, Cvec3 p0, Cvec3 n, Cvec3* intersection);

////PHYSICS SIMULATION
PhysicsSim::PhysicsSim(shared_ptr<SgRootNode> rootNode) :
	sphericalBodies_(),
	planarBodies_(),
	rootNode_(rootNode),
	damping_(0.0f)
 {

	
}

PhysicsSim::~PhysicsSim() {

}

bool PhysicsSim::sphereCollision(int s1, int s2) {

	shared_ptr<SgTransformNode> n1 = dynamic_pointer_cast<SgTransformNode>(sphericalBodies_[s1]);
	shared_ptr<SgTransformNode> n2 = dynamic_pointer_cast<SgTransformNode>(sphericalBodies_[s2]);

	RigTForm pos1 = getPathAccumRbt(rootNode_, n1);
	RigTForm pos2 = getPathAccumRbt(rootNode_, n2);

	SphericalPhysicsBody *sphere1 = dynamic_cast<SphericalPhysicsBody *>(sphericalBodies_[s1]->getPhysicsBody());
	SphericalPhysicsBody *sphere2 = dynamic_cast<SphericalPhysicsBody *>(sphericalBodies_[s2]->getPhysicsBody());

	double rad1 = sphere1->getRadius(); 
	double rad2 = sphere2->getRadius();

	double dist = norm(pos2.getTranslation() - pos1.getTranslation());
	if(dist <= rad1 + rad2) {

		//corrections, keep spheres from intersecting
		float offset = (rad1 + rad2 - dist);
		Cvec3 fromS1ToS2 = normalize(pos2.getTranslation() - pos1.getTranslation());
		fromS1ToS2 *= offset;
		n2->setRbt(RigTForm(fromS1ToS2) * n2->getRbt());

		return true;
	} else
		return false;
}

void PhysicsSim::updateSphereSphereVelocities(int s1, int s2) {

	shared_ptr<SgTransformNode> n1 = dynamic_pointer_cast<SgTransformNode>(sphericalBodies_[s1]);
	shared_ptr<SgTransformNode> n2 = dynamic_pointer_cast<SgTransformNode>(sphericalBodies_[s2]);

	Cvec3 pos1 = getPathAccumRbt(rootNode_, n1).getTranslation();
	Cvec3 pos2 = getPathAccumRbt(rootNode_, n2).getTranslation();

	SphericalPhysicsBody *sphere1 = dynamic_cast<SphericalPhysicsBody *>(sphericalBodies_[s1]->getPhysicsBody());
	SphericalPhysicsBody *sphere2 = dynamic_cast<SphericalPhysicsBody *>(sphericalBodies_[s2]->getPhysicsBody());

	Cvec3 u1 = sphere1->getVelocity();
	Cvec3 u2 = sphere2->getVelocity();

	double m1 = sphere1->getMass();
	double m2 = sphere2->getMass();

	Cvec3 pos = pos1 - pos2;

	// if positions are too close, choose an arbitrary vector?
	Cvec3 k = Cvec3(1,0,0);

	if (dot(pos, pos) > ((1e-8) * (1e-8))) {
		k = pos.normalize();
	}

	double a = (dot((k * 2.0), (u1 - u2)) * m1 * m2) / (m1 + m2);

	Cvec3 v1 = u1 - (k * (a / m1));
	Cvec3 v2 = u2 + (k * (a / m2));
 
	sphere1->setVelocity(v1 * (1 - damping_));
	sphere2->setVelocity(v2 * (1 - damping_));
}

void PhysicsSim::updateSpherePlaneVelocities(int s, int p) {

	SphericalPhysicsBody *sphere = dynamic_cast<SphericalPhysicsBody *>(sphericalBodies_[s]->getPhysicsBody());
	
	PlanarPhysicsBody *plane = dynamic_cast<PlanarPhysicsBody *>(planarBodies_[p]->getPhysicsBody());
	shared_ptr<SgTransformNode> planeNode = dynamic_pointer_cast<SgTransformNode>(planarBodies_[p]);
	
	RigTForm planeRtf = getPathAccumRbt(rootNode_, planeNode);
	Cvec3 planeNormal = planeRtf.getRotation() * plane->getUnitNormal();
	Cvec3 velocity = sphere->getVelocity();
	planeNormal.normalize();
	Cvec3 velDiff = -(planeNormal * (2 * dot(velocity, planeNormal)));

	Cvec3 newVelocity = velocity + velDiff;
	newVelocity *= (1 - damping_);

	sphere->setVelocity(newVelocity);
}

void PhysicsSim::update(float timeElapsed) {
	
	int numSpheres = sphericalBodies_.size();
	int numPlanes = planarBodies_.size();

	// update position of each object based on its velocity
	for (int i = 0; i < numSpheres; ++i) {

		SphericalPhysicsBody *sphere = dynamic_cast<SphericalPhysicsBody *>(sphericalBodies_[i]->getPhysicsBody());
		Cvec3 velocity = sphere->getVelocity();
		velocity += sphere->getAcceleration() * timeElapsed;
		sphere->setVelocity(velocity);

		RigTForm pos = sphericalBodies_[i]->getRbt();
		RigTForm trans = RigTForm(velocity * timeElapsed); // replace with timeStep?
		RigTForm newRigTForm = trans * pos;
		sphericalBodies_[i]->setRbt(trans * pos);

	}

	for (int k = 0; k < numSpheres; k++) {
		// don't check any pair twice!
		for (int j = k + 1; j < numSpheres; j++) {
			if (sphereCollision(k, j)) {
				// check if there's a collision between the two spheres
				updateSphereSphereVelocities(k, j);
			}
		}

		for (int m = 0; m < numPlanes; m++) {
			Cvec3 intersectionPoint;
			if (intersect_sphere_plane(sphericalBodies_[k], planarBodies_[m], &intersectionPoint)) {
				updateSpherePlaneVelocities(k, m);
			}
		}
	}
}

void PhysicsSim::add(const shared_ptr<HasPhysicsBody>& hpb) {

	PhysicsBody *pb = hpb->getPhysicsBody();
	
	//check kind of body
	if(SphericalPhysicsBody *spb = dynamic_cast<SphericalPhysicsBody *>(pb)) {

		sphericalBodies_.push_back(hpb);

	} else if(PlanarPhysicsBody *spb = dynamic_cast<PlanarPhysicsBody *>(pb)) {

		planarBodies_.push_back(hpb);

	} else {
		//don't add to sim
		std::cout << "Added nothing!" << std::endl;
	}
}

//true on intersection, will populate passed Cvec3 pointer with intersection point
bool PhysicsSim::intersect_sphere_plane(shared_ptr<HasPhysicsBody> sphere, shared_ptr<HasPhysicsBody> plane, Cvec3* intersectionPoint) {

	SphericalPhysicsBody *spherePb = dynamic_cast<SphericalPhysicsBody *>(sphere->getPhysicsBody());
	shared_ptr<SgTransformNode> sphereNode = dynamic_pointer_cast<SgTransformNode>(sphere);

	PlanarPhysicsBody *planePb = dynamic_cast<PlanarPhysicsBody *>(plane->getPhysicsBody());
	shared_ptr<SgTransformNode> planeNode = dynamic_pointer_cast<SgTransformNode>(plane);

	//assert sphere and plane!
	assert(spherePb != NULL && sphereNode != nullptr);
	assert(planePb != NULL && planeNode != nullptr);

	Cvec3 sphereCenter = getPathAccumRbt(rootNode_, sphereNode).getTranslation();
	
	RigTForm planeRtf = getPathAccumRbt(rootNode_, planeNode);

	
	Cvec3 planeNormal = planeRtf.getRotation() * planePb->getUnitNormal();
	//line is a line perp. to plane passing through sphere center

	Cvec3 intersection = Cvec3();
	bool doesIntersect =
		intersect_line_plane(
			sphereCenter,
			planeNormal,
			planeRtf.getTranslation(),
			planeNormal,
			&intersection
		);
	//constructed to be perp.
	assert(doesIntersect);
	Cvec3 perpVecFromCtrToPlane = intersection - sphereCenter;

	float distFromCtrToPlane = norm(perpVecFromCtrToPlane);

	//if the closest dist. from the center of the sphere to the plane
	//is less than radius, then collision. Otherwise, no.
	if(distFromCtrToPlane <= spherePb->getRadius()) {

		//TODO: check that intersection lies within bounds of finite plane
		//first: find quat from x-axis to the normal that has had the RigTForm's quat applied
		//use this quat to transform the y and z axes
		//get the intersection point relative to planar center
		//apply the inverse quat from the first part
		//check that abs(y) and ans(z) components are less than width/2 and height/2!!
		Quat fromXAxisToNormal;
		if(abs(dot(planeNormal, Cvec3(1, 0, 0))) - 1 < CS175_EPS2) {
			//normal is parallel to x-axis
			//Case 1: opposite
			if(dot(planeNormal, Cvec3(1, 0, 0)) < 0) {
				fromXAxisToNormal = Quat::makeZRotation(2 * M_PI);

			} else {//Case 2:on the axis
				fromXAxisToNormal = Quat::makeZRotation(0);
			}
		} else {
			//then it's valid to do general formula
			//from v1 to v2
			Cvec3 a = cross(Cvec3(1, 0, 0), planeNormal);
			fromXAxisToNormal[0] = a[0];
			fromXAxisToNormal[1] = a[1];
			fromXAxisToNormal[2] = a[2];
			fromXAxisToNormal[3] = sqrt(norm2(planeNormal)) + planeNormal[0];
		}
		Quat fromNormalToXAxis = inv(fromXAxisToNormal);
		Cvec3 intersectionOnXAxis = fromNormalToXAxis * (intersection - planeRtf.getTranslation());
		if(abs(intersectionOnXAxis[1] <= planePb->getWidth()/2 && intersectionOnXAxis[2] <= planePb->getHeight()/2)) {

			//fix sphere position to be firmly outside of plane!
			float overlap = spherePb->getRadius() - distFromCtrToPlane;
			Cvec3 offset = planeNormal * overlap;
			sphereNode->setRbt(sphereNode->getRbt() * RigTForm(offset));

			//intersection within bounds!
			(*intersectionPoint) = intersection;
			return true;
		} else
			return false;
	} else
		return false;
}

//The line is parallel to l and passes through l0
//The plane passes through p0 and has n as a normal
//will fill intersection if true
//if they are parallel and the line is not on the plane, then false
//otherwise true, and intersection is populated by p0
static bool intersect_line_plane(Cvec3 l0, Cvec3 l, Cvec3 p0, Cvec3 n, Cvec3* intersection) {

	float numerator = dot((p0 - l0), n);
	float lDotN = dot(l, n);

	if(lDotN < CS175_EPS2) {
		//line and plane are parallel
		if(numerator < CS175_EPS2) {
			//line is in plane
			(*intersection)[0] = p0[0];
			(*intersection)[1] = p0[1];
			(*intersection)[2] = p0[2];
			return true;
		} else {
			return false;
		}
	}else{

		float t = numerator / lDotN;
		Cvec3 point = (l * t) + l0;
		(*intersection)[0] = point[0];
		(*intersection)[1] = point[1];
		(*intersection)[2] = point[2];

		return true;
	}
}


