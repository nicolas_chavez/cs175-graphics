
////////////////////////////////////////////////////////////////////////
//
//   Harvard University
//   CS175 : Computer Graphics
//   Professor Steven Gortler
//
////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <memory>
#include <stdexcept>

#ifdef __MAC__
#   include <OpenGL/gl3.h>
#   include <GLUT/glut.h>
#else
#   include <GL/glew.h>
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "geometrymaker.h"
#include "ppm.h"
#include "glsupport.h"
#include "rigtform.h"
#include "arcball.h"
#include "asstcommon.h"
#include "scenegraph.h"
#include "drawer.h"
#include "picker.h"
#include "geometry.h"
#include "physicsSim.h"

using namespace std;  // for string, vector, iostream, and other standard C++ stuff

// G L O B A L S ///////////////////////////////////////////////////
static int g_activeShader = 0;
static int g_skyModMode = 0; //0 means world sky frame, 1 means sky sky frame

static bool g_arcballVisible = true;
static bool g_aboutToPick = false;

static vector<shared_ptr<ShaderState> > g_shaderStates; // our global shader states

// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground;
static shared_ptr<Geometry> g_cube;
static shared_ptr<Geometry> g_sphere;

typedef SgGeometryShapeNode<Geometry> MyShapeNode;

static shared_ptr<SgRootNode> g_world;
static shared_ptr<SgRbtNode> g_skyNode, g_groundNode;
static shared_ptr<SgRbtNode> g_currentPickedRbtNode = g_skyNode; // used later when you do picking
static shared_ptr<SgRbtNode> g_eyeFrameNode = g_skyNode;

// ---Physics Simulation
static float g_simUpdateFreq = (float)1/(float)10000;
static PhysicsSim *g_physSim = NULL;
static double g_density = 1.0;
static int g_numSpheres = 10;
static bool g_pausePhysicsSim = false;
static bool g_pickingAllowed = true;
static int g_currentSphere = -1;
static bool g_gravityDamping = false;

// --------- Scene
static const Cvec3
  g_light1(2.0, 3.0, 14.0),
  g_light2(-2, -3.0, -5.0);  // define two lights positions in world space

static RigTForm g_aFrame = RigTForm();



///////////////// END OF G L O B A L S //////////////////////////////////////////////////

static void initGround() {
  // A x-z plane at y = g_groundY of dimension [-g_groundSize, g_groundSize]^2
  VertexPN vtx[4] = {
    VertexPN(-g_groundSize, 0, -g_groundSize, 0, 1, 0),
    VertexPN(-g_groundSize, 0,  g_groundSize, 0, 1, 0),
    VertexPN( g_groundSize, 0,  g_groundSize, 0, 1, 0),
    VertexPN( g_groundSize, 0, -g_groundSize, 0, 1, 0),
  };
  unsigned short idx[] = {0, 1, 2, 0, 2, 3};
  g_ground.reset(new Geometry(&vtx[0], &idx[0], 4, 6));
}

static void initCube() {

  int ibLen, vbLen;
  getCubeVbIbLen(vbLen, ibLen);

  // Temporary storage for cube geometry
  vector<VertexPN> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeCube(1, vtx.begin(), idx.begin());

  g_cube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}

static void initSphere() {

  int ibLen, vbLen;
  int slices = 20, stacks = 20;
  //getCubeVbIbLen(vbLen, ibLen);
  getSphereVbIbLen(slices, stacks, vbLen, ibLen);

  // Temporary storage for sphere geometry
  vector<VertexPN> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeSphere(1.0, slices, stacks, vtx.begin(), idx.begin());
  
  g_sphere.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}

static void buildAFrame(){

  if (g_eyeFrameNode == g_skyNode && g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0) {
    Quat rot = getPathAccumRbt(g_world, g_currentPickedRbtNode).getRotation();
    g_aFrame = RigTForm(Cvec3(), rot);

  } else {
    Cvec3 trans = getPathAccumRbt(g_world, g_currentPickedRbtNode).getTranslation();
    Quat rot = getPathAccumRbt(g_world, g_eyeFrameNode).getRotation();
    g_aFrame = RigTForm(trans, rot);
  }
}

static RigTForm makeArcballRbt() {

  RigTForm arcballRbt;
  if (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0) {
    arcballRbt = getPathAccumRbt(g_world, g_world);
  } else {
    arcballRbt = getPathAccumRbt(g_world, g_currentPickedRbtNode);
  }

  g_arcballVisible = (g_pickingAllowed && (g_currentPickedRbtNode != g_eyeFrameNode || (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0)));
  return arcballRbt;
}

static void drawStuff(const ShaderState& curSS, bool picking) {

  // build & send proj. matrix to vshader
  const Matrix4 projmat = makeProjectionMatrix();
  sendProjectionMatrix(curSS, projmat);

  // choose frame to use as eyeRbt
  RigTForm eyeRbt = getPathAccumRbt(g_world, g_eyeFrameNode);
  
  const RigTForm invEyeRbt = inv(eyeRbt);

  const Cvec3 eyeLight1 = Cvec3(invEyeRbt * Cvec4(g_light1, 1)); // g_light1 position in eye coordinates
  const Cvec3 eyeLight2 = Cvec3(invEyeRbt * Cvec4(g_light2, 1)); // g_light2 position in eye coordinates
  safe_glUniform3f(curSS.h_uLight, eyeLight1[0], eyeLight1[1], eyeLight1[2]);
  safe_glUniform3f(curSS.h_uLight2, eyeLight2[0], eyeLight2[1], eyeLight2[2]);

  RigTForm arcballRbt = makeArcballRbt();

  if(!(g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton) || (g_mouseLClickButton && !g_mouseRClickButton && g_spaceDown))){

    // update arcballScale
    g_arcballScale = getScreenToEyeScale((invEyeRbt * arcballRbt).getTranslation()[2], g_frustFovY, g_windowHeight);
  }

  if (!picking) {

    Drawer drawer(invEyeRbt, curSS);
    g_world->accept(drawer);

    // draw arcball as part of asst3
    if (g_arcballVisible) {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // draw wireframe
  
      double s = g_arcballScale * g_arcballScreenRadius;

      RigTForm MVMrbt = invEyeRbt * arcballRbt;
      Matrix4 MVM = rigTFormToMatrix(MVMrbt);
      MVM = MVM * Matrix4::makeScale(Cvec3(s, s, s));
  
      Matrix4 NMVM = normalMatrix(MVM);
      sendModelViewNormalMatrix(curSS, MVM, NMVM);
      //purp
      safe_glUniform3f(curSS.h_uColor, 0.9, 0.1, 0.9);
      g_sphere->draw(curSS);
  
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    

  } else {//if indeed picking

    Picker picker(invEyeRbt, curSS);
    g_world->accept(picker);
    glFlush();

    g_currentPickedRbtNode = picker.getRbtNodeAtXY(g_mouseClickX, g_mouseClickY);

    if (!g_currentPickedRbtNode)
      g_currentPickedRbtNode = g_eyeFrameNode;

    g_arcballVisible = (g_pickingAllowed && (g_currentPickedRbtNode != g_eyeFrameNode || (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0)));
    g_arcballScale = getScreenToEyeScale((invEyeRbt * arcballRbt).getTranslation()[2], g_frustFovY, g_windowHeight);
  } 
}

static void pick() {

  if(!g_pickingAllowed)
    return;

  g_aboutToPick = false;
  // We need to set the clear color to black, for pick rendering.
  // so let's save the clear color
  GLdouble clearColor[4];
  glGetDoublev(GL_COLOR_CLEAR_VALUE, clearColor);

  glClearColor(0, 0, 0, 0);

  // using PICKING_SHADER as the shader
  glUseProgram(g_shaderStates[PICKING_SHADER]->program);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  drawStuff(*g_shaderStates[PICKING_SHADER], true);

  glutPostRedisplay();

  // Uncomment below and comment out the glutPostRedisplay in mouse(...) call back
  // to see result of the pick rendering pass
  //glutSwapBuffers();

  //Now set back the clear color
  glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);

  checkGlErrors();
}

static void updateSimulation(int dontCare) {

  float timeElapsed = g_simUpdateFreq * (float)1000;
  
  if (g_physSim != NULL && !g_pausePhysicsSim) {
    
    g_physSim->update(timeElapsed);
    glutPostRedisplay();
  }

  //repeat callback
  glutTimerFunc(1, updateSimulation, dontCare);
}

static void display() {
  glUseProgram(g_shaderStates[g_activeShader]->program);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                   // clear framebuffer color&depth

  drawStuff(*g_shaderStates[g_activeShader], false);

  glutSwapBuffers();                                    // show the back buffer (where we rendered stuff)

  checkGlErrors();
}

static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  g_arcballScreenRadius = 0.25 * min(g_windowHeight, g_windowWidth);
  glViewport(0, 0, w, h);
  cerr << "Size of window is now " << w << "x" << h << endl;
  updateFrustFovY();
  glutPostRedisplay();
}

static Quat arcballRotation(const int x, const int y){

  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  RigTForm eyeRbt = getPathAccumRbt(g_world, g_eyeFrameNode);
  RigTForm arcballRbt = makeArcballRbt();

  RigTForm invEyeRbt = inv(eyeRbt);
  Cvec2 arcballScreenCenter = getScreenSpaceCoord((invEyeRbt * arcballRbt).getTranslation(), makeProjectionMatrix(), g_frustNear, g_frustFovY, g_windowWidth, g_windowHeight);

  double x1 = g_mouseClickX - arcballScreenCenter[0];
  double y1 = g_mouseClickY - arcballScreenCenter[1];
  double z1;
  Cvec3 v1;

  double x2 = x - arcballScreenCenter[0];
  double y2 = g_windowHeight - y - 1 - arcballScreenCenter[1];
  double z2;
  Cvec3 v2;

  if (pow(x1, 2) + pow(y1, 2) > pow(g_arcballScreenRadius, 2)) {
    z1 = 0;
    v1 = normalize(Cvec3(x1, y1, z1)) * g_arcballScreenRadius;
  } else {
      z1 = sqrt(pow(g_arcballScreenRadius, 2) - pow(x1, 2) - pow(y1, 2));
      v1 = Cvec3(x1, y1, z1);
  }
  if (pow(x2, 2) + pow(y2, 2) > pow(g_arcballScreenRadius, 2)) {
    z2 = 0;
    v2 = normalize(Cvec3(x2, y2, z2)) * g_arcballScreenRadius;
  } else {
      z2 = sqrt(pow(g_arcballScreenRadius, 2) - pow(x2, 2) - pow(y2, 2));
      v2 = Cvec3(x2, y2, z2);
  }

  v1 = normalize(v1);
  v2 = normalize(v2);

  //Quat rot = Quat(0, v2) * Quat(0, -v1);
  Quat rot = Quat(dot(v1, v2), cross(v1, v2));

  return rot;
}

static void updateSphereView() {

  if(g_currentSphere < -1)
    g_currentSphere = g_physSim->getNumSpheres() - 1;

  else if(g_currentSphere >= g_physSim->getNumSpheres())
    g_currentSphere = -1;

  if(g_currentSphere == -1) {
    g_eyeFrameNode = g_skyNode;
    g_pickingAllowed = true;

  } else {
    shared_ptr<SgRbtNode> sphereNode = dynamic_pointer_cast<SgRbtNode>(g_physSim->getSphereAtIndex(g_currentSphere));
    
    if(sphereNode != nullptr) {
      g_eyeFrameNode = sphereNode;
      g_pickingAllowed = false;
    } else {
      g_eyeFrameNode = g_skyNode;
      g_pickingAllowed = true;
    }   
  }

  if(g_currentSphere == -1)
    cout << "SkyNode!" << endl;
  else
    cout << "Sphere " << (g_currentSphere + 1) << endl;
}

static void sphereAction() {

  if(g_currentSphere >= 0 && g_currentSphere < g_physSim->getNumSpheres()) {

    shared_ptr<SgRbtNode> sphereNode = dynamic_pointer_cast<SgRbtNode>(g_physSim->getSphereAtIndex(g_currentSphere));
    if(sphereNode != nullptr) {
      //do something cool!
    }
  }
}

static void toggleGravity() {

  if(g_physSim == NULL) return;

  g_gravityDamping = !g_gravityDamping;

  Cvec3 accel = Cvec3();
  if(g_gravityDamping)
    accel = Cvec3(0, -0.2f, 0);

  for(int i = 0; i < g_physSim->getNumSpheres(); ++i) {
    shared_ptr<HasPhysicsBody> hpb = g_physSim->getSphereAtIndex(i);
    PhysicsBody *pb = hpb->getPhysicsBody();
    if(pb != NULL)
      pb->setAcceleration(accel);
  }

  g_physSim->setDamping(g_gravityDamping ? 0.1f : 0.0f);
}

static void motion(const int x, const int y) {
  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  Matrix4 m;
  RigTForm mRbt;
  
  if(!g_pickingAllowed) return;

  if (g_mouseLClickButton && !g_mouseRClickButton && !g_spaceDown) { // left button down?

    if (g_arcballVisible){
      mRbt = RigTForm(arcballRotation(x, y));
    } else {
      mRbt = RigTForm(Quat::makeXRotation(-dy) * Quat::makeYRotation(dx));
    }
  }
  else if (g_mouseRClickButton && !g_mouseLClickButton) { // right button down?

    if (g_arcballVisible) {
      mRbt = RigTForm(Cvec3(dx, dy, 0) * g_arcballScale);
    } else {
      mRbt = RigTForm(Cvec3(dx, dy, 0) * 0.01);
    }
    
  }
  else if (g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton) || (g_mouseLClickButton && !g_mouseRClickButton && g_spaceDown)) {  // middle or (left and right, or left + space) button down?

    if (g_arcballVisible) {
      mRbt = RigTForm(Cvec3(0, 0, -dy) * g_arcballScale);
    } else {
      mRbt = RigTForm(Cvec3(0, 0, -dy) * 0.01);
    }
    
  }

  buildAFrame();

  if (g_mouseClickDown) {
    
    RigTForm curRbt = g_currentPickedRbtNode->getRbt();

    if(g_currentPickedRbtNode == g_skyNode) {

      if (g_skyModMode == 0) {
          Quat rot = mRbt.getRotation();
          mRbt = RigTForm(-(mRbt.getTranslation()), Quat(rot[0], -(rot[1]), -(rot[2]), -(rot[3])));
          g_skyNode->setRbt(g_aFrame * mRbt * inv(g_aFrame) * curRbt);
      } else {
          Quat rot = mRbt.getRotation();
          mRbt = RigTForm(mRbt.getTranslation(), Quat(rot[0], -(rot[1]), -(rot[2]), -(rot[3])));
          g_skyNode->setRbt(g_aFrame * mRbt * inv(g_aFrame) * curRbt);
      }
    }else {
      
      //g_arcballRbt = g_aFrame * mRbt * inv(g_aFrame) * g_arcballRbt;
      RigTForm newAFrame = inv(getPathAccumRbt(g_world, g_currentPickedRbtNode, 1)) * g_aFrame;
      g_currentPickedRbtNode->setRbt(newAFrame * mRbt * inv(newAFrame) * curRbt);
    } 
    glutPostRedisplay(); // we always redraw if we changed the scene
  }

  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;
}

static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;

  ///

  if(g_mouseLClickButton){

    if(g_aboutToPick){
      pick();
    }
  }
}

static void keyboardUp(const unsigned char key, const int x, const int y) {
  switch (key) {
  case ' ':
    g_spaceDown = false;
    break;
  }
  glutPostRedisplay();
}

static void keyboard(const unsigned char key, const int x, const int y) {
  switch (key) {
  case 27:
    exit(0);                                  // ESC
  case 'h':
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "s\t\tsave screenshot\n"
    << "f\t\tToggle flat shading on/off.\n"
    << "o\t\tCycle object to edit\n"
    << "v\t\tCycle view\n"
    << "drag left mouse to rotate\n" << endl;
    break;
  case 's':
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    break;
  case 'f':
    g_activeShader ^= 1;
    break;

  case 'p':
    g_aboutToPick = true;
    break;
  case 'v':    
    g_eyeFrameNode = g_skyNode;
    g_currentPickedRbtNode = g_skyNode;
    makeArcballRbt();
    cout << "current view: sky camera" << endl; 
    //resetArcball();
    break;
  case 'm':
      g_skyModMode = (g_skyModMode + 1) % 2;
      switch(g_skyModMode){
        case 0:
          cout << "sky-world" << endl;
          break;
        case 1:
          cout << "sky-sky" << endl;
          break;
      }
    //resetArcball();
    break;
  case 'k':
    if(!g_pausePhysicsSim)
      cout << "Paused physics simulation!" << endl;
    else
      cout << "Resumed physics simulation!" << endl;
    g_pausePhysicsSim = !g_pausePhysicsSim;
  break;
  case 'q':
    --g_currentSphere;
    updateSphereView();
  break;
  case 'w':
    ++g_currentSphere;
    updateSphereView();
  break;
  case 'e':
    sphereAction();
  break;
  case 'g':
    toggleGravity();
  break;
  case ' ':
    g_spaceDown = true;
    break;
  }
  glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
#ifdef __MAC__
  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE|GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH); // core profile flag is required for GL 3.2 on Mac
#else
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
#endif
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("Final Project");                      // title the window

  glutIgnoreKeyRepeat(true);                              // avoids repeated keyboard calls when holding space to emulate middle mouse

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutMotionFunc(motion);                                 // mouse movement callback
  glutMouseFunc(mouse);                                   // mouse click callback
  glutKeyboardFunc(keyboard);
  glutKeyboardUpFunc(keyboardUp);
}

static void initGLState() {
  glClearColor(128./255., 200./255., 255./255., 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initShaders() {
  g_shaderStates.resize(g_numShaders);
  for (int i = 0; i < g_numShaders; ++i) {
    if (g_Gl2Compatible)
      g_shaderStates[i].reset(new ShaderState(g_shaderFilesGl2[i][0], g_shaderFilesGl2[i][1]));
    else
      g_shaderStates[i].reset(new ShaderState(g_shaderFiles[i][0], g_shaderFiles[i][1]));
  }
}

static void initGeometry() {
  initGround();
  initCube();
  initSphere();
}

// generates random-ish radius and velocity for a sphere
static void createSphereforPhysicsSim(float* radius, Cvec3* velocity, Cvec3* translation, Cvec3* color) {

  (*radius) = ((rand() % 4) + 1) / 4.0;

  double v1 = (rand() % 20) - 10;
  double v2 = (rand() % 20) - 10;
  double v3 = (rand() % 20) - 10;
  Cvec3 v = Cvec3(v1, v2, v3);
  v.normalize();

  v *= ((rand() % 10) / 5.0);

  (*velocity) = v;

  // figure out what range of starting positions makes the sphere visible in the scene!
  double t1 = (rand() % 12) - 6;
  double t2 = (rand() % 6);
  double t3 = (rand() % 12) - 6;
  Cvec3 t = Cvec3(t1, t2, t3);
  (*translation) = t;

  double c1 = (float)rand()/(float)RAND_MAX;
  double c2 = (float)rand()/(float)RAND_MAX;
  double c3 = (float)rand()/(float)RAND_MAX;
  Cvec3 c = Cvec3(c1, c2, c3);
  (*color) = c;
}

static void addWalls() {
  
  float wallDist = 8;

  Cvec3 trans[5] =
  {
    Cvec3(0, +1, 0),
    Cvec3(0, 0, -1),
    Cvec3(0, 0, +1),
    Cvec3(+1, 0, 0),
    Cvec3(-1, 0, 0),
  };
  Quat quats[5] = 
  {
    Quat::makeXRotation(180),
    Quat::makeXRotation(90),
    Quat::makeXRotation(270),
    Quat::makeZRotation(90),
    Quat::makeZRotation(270),
  };
  Cvec3 colors[5] = 
  {
    Cvec3(1, .8, .8),
    Cvec3(1, .8, .8),
    Cvec3(1, .8, .8),
    Cvec3(1, .8, .8),
    Cvec3(1, .8, .8),
  };


  for (int i = 0; i < 5; ++i) {

    shared_ptr<SgRbtNode> wall(new SgRbtNode(RigTForm(trans[i] * wallDist, quats[i])));
    wall->addChild(shared_ptr<MyShapeNode>(
                           new MyShapeNode(
                            g_ground,
                            colors[i]
                          )));
    PhysicsBody *wallPb = new PlanarPhysicsBody(Cvec3(0, 1, 0), 2 * g_groundSize, 2 * g_groundSize);
    wall->setPhysicsBody(wallPb);
    g_physSim->add(wall);
    g_world->addChild(wall);
  }
}

static void initScene() {
  g_world.reset(new SgRootNode());

  g_physSim = new PhysicsSim(g_world);

  g_skyNode.reset(new SgRbtNode(RigTForm(Cvec3(0.0, 0.25, 15.0))));

  g_currentPickedRbtNode = g_skyNode;
  g_eyeFrameNode = g_skyNode;

  g_groundNode.reset(new SgRbtNode(RigTForm(Cvec3(0, g_groundY, 0))));
  g_groundNode->addChild(shared_ptr<MyShapeNode>(
                           new MyShapeNode(
                            g_ground,
                            Cvec3(0.1, 0.95, 0.1)
                          )));
  
  PhysicsBody *groundPb = new PlanarPhysicsBody(Cvec3(0, 1, 0), 2 * g_groundSize, 2 * g_groundSize);
  g_groundNode->setPhysicsBody(groundPb);
  g_physSim->add(g_groundNode);

  // add a loop here to make a bunch of spheres of different radii
  //seed rand with timestamp
  unsigned int time_ui = static_cast<unsigned int>( time(NULL)%1000 );
  srand( time_ui );
  
  for (int i = 0; i < g_numSpheres; i++) {
    float radius;
    Cvec3 velocity;
    Cvec3 translation;
    Cvec3 color;
    createSphereforPhysicsSim(&radius, &velocity, &translation, &color);
    double mass = (4 / 3.0) * M_PI * pow(radius, 3) * g_density;

    shared_ptr<SgRbtNode> sphere(new SgRbtNode(RigTForm(translation)));
    sphere->addChild(shared_ptr<MyShapeNode>(
    new MyShapeNode(
      g_sphere,
      color,
      Cvec3(0, 0, 0),
      Cvec3(0, 0, 0),
      Cvec3(radius, radius, radius)
    )
    ));
    PhysicsBody *spherePb = new SphericalPhysicsBody(radius);
    spherePb->setMass(mass);
    spherePb->setVelocity(velocity);
    spherePb->setAcceleration(Cvec3(0,0,0)); //Cvec3(0,-0.2f,0));
    spherePb->setStatic(false);

    g_world->addChild(sphere);

    sphere->setPhysicsBody(spherePb);
    g_physSim->add(sphere);
  }

  addWalls();

  g_world->addChild(g_skyNode);
  g_world->addChild(g_groundNode);
}

static void initPhysicsSimulation() {

  glutTimerFunc(g_simUpdateFreq * 1000, updateSimulation, 0);
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

  // on Mac, we shouldn't use GLEW.

#ifndef __MAC__
    glewInit(); // load the OpenGL extensions
#endif

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.5") << endl;

#ifndef __MAC__
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");
#endif

    initGLState();
    initShaders();
    initGeometry();
    initScene();
    initPhysicsSimulation();

    glutMainLoop();
    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
