#ifndef _PHYSICS_BODY_H__
#define _PHYSICS_BODY_H__

#include "cvec.h"
#include "rigtform.h"

/*
	Used to simulate physics objects individually
*/
class PhysicsBody {

public:
	PhysicsBody();
	virtual ~PhysicsBody() {}

	double getMass() { return mass_; }
	void setMass(double m) { mass_ = m; }

	Cvec3 getVelocity() { return vel_; }
	void setVelocity(const Cvec3& v) { vel_ = v; }

	Cvec3 getAcceleration() { return accel_; }
	void setAcceleration(const Cvec3& a) { accel_ = a; }

	bool isStatic() { return isStatic_; }
	void setStatic(bool isStatic) { isStatic_ = isStatic; }

protected:
	Cvec3 vel_, accel_;
	double mass_;
	bool isStatic_;
};

/*
	Spherically shaped PhysicsBody, given a radius
*/
class SphericalPhysicsBody : public PhysicsBody {

public:
	SphericalPhysicsBody(float radius) : radius_(radius) {}

	double getRadius() { return radius_; }
	
	virtual ~SphericalPhysicsBody() {}

protected:
	float radius_;
};
/*
	Defined by a unit normal and plane size
	The unit normal is treated as the positive x-axis,
	width is determined along the corresponding y
	and height as the corresponding z
	This is then transformed by the HasPhysicsBody's RBT
*/
class PlanarPhysicsBody : public PhysicsBody {

public:
	PlanarPhysicsBody(Cvec3 unitNorm, float width, float height)
	: unitNorm_(unitNorm), width_(width), height_(height)
	{}
	virtual ~PlanarPhysicsBody() {}

	virtual Cvec3 getUnitNormal() { return unitNorm_; }
	virtual float getWidth() { return width_; }
	virtual float getHeight() {return height_; }

protected:
	Cvec3 unitNorm_;
	float width_, height_;
};

/*
	Interface to wrap SgTransformNodes to contain physicsBodies
*/
class HasPhysicsBody {
public:
	virtual RigTForm getRbt() { return RigTForm(); }
  	virtual void setRbt(const RigTForm& rbt) { }

	virtual PhysicsBody* getPhysicsBody() { return NULL; }
};

#endif//_PHYSICS_BODY_H__
