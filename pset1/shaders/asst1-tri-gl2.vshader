uniform float uWindowScaleX;
uniform float uWindowScaleY;
uniform float uTriangleMoveX;
uniform float uTriangleMoveY;

attribute vec2 aPosition;
attribute vec2 aTexCoord;
attribute vec3 aColorRGB;

varying vec2 vTexCoord;
varying vec3 vColorRGB;

void main() {
  gl_Position = vec4((aPosition.x + uTriangleMoveX) * uWindowScaleX, (aPosition.y + uTriangleMoveY) * uWindowScaleY, 1, 1);
  vTexCoord = aTexCoord;
  vColorRGB = aColorRGB;
}
