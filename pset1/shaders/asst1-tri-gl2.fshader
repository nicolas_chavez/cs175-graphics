uniform sampler2D uTex0;

varying vec2 vTexCoord;
varying vec3 vColorRGB;

void main(void) {
  
  vec4 color = vec4(vColorRGB.x, vColorRGB.y, vColorRGB.z, 1.0);
  vec4 texColor0 = texture2D(uTex0, vTexCoord);
  
  float colorProp = 0.5;

  // gl_FragColor is a vec4. The components are interpreted as red, green, blue, and alpha
  gl_FragColor = colorProp*color + (1.0-colorProp)*texColor0;
}
