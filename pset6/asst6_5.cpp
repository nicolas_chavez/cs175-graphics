
////////////////////////////////////////////////////////////////////////
//
//   Harvard University
//   CS175 : Computer Graphics
//   Professor Steven Gortler
//
////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <memory>
#include <stdexcept>
#if __GNUG__
#   include <tr1/memory>
#endif

#ifdef __MAC__
#   include <OpenGL/gl3.h>
#   include <GLUT/glut.h>
#else
#   include <GL/glew.h>
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "geometrymaker.h"
#include "ppm.h"
#include "glsupport.h"
#include "rigtform.h"
#include "arcball.h"
#include "asstcommon.h"
#include "scenegraph.h"
#include "drawer.h"
#include "picker.h"
#include "sgutils.h"
#include "geometry.h"
#include "animation.h"

using namespace std;  // for string, vector, iostream, and other standard C++ stuff
using namespace tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

static const float g_frustMinFov = 60.0;  // A minimal of 60 degree field of view
static float g_frustFovY = g_frustMinFov; // FOV in y direction (updated by updateFrustFovY)

static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 10.0;   // half the ground length

static int g_windowWidth = 512;
static int g_windowHeight = 512;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static bool g_spaceDown = false;         // space state, for middle mouse emulation
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event
static int g_activeShader = 0;
static int g_skyModMode = 0; //0 means world sky frame, 1 means sky sky frame

static double g_arcballScreenRadius = 0.25 * 512;
static double g_arcballScale = 1.0;
static bool g_arcballVisible = true;
static bool g_aboutToPick = false;

static const int PICKING_SHADER = 2; // index of the picking shader is g_shaderFiles
static shared_ptr<Material> g_redDiffuseMat,
                            g_blueDiffuseMat,
                            g_bumpFloorMat,
                            g_arcballMat,
                            g_pickingMat,
                            g_lightMat;

shared_ptr<Material> g_overridingMaterial;

// --------- Geometry
typedef SgGeometryShapeNode MyShapeNode;

// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground;
static shared_ptr<Geometry> g_cube;
static shared_ptr<Geometry> g_sphere;

static shared_ptr<SgRootNode> g_world;
static shared_ptr<SgRbtNode> g_skyNode, g_groundNode, g_robot1Node, g_robot2Node, g_light1Node, g_light2Node;
static shared_ptr<SgRbtNode> g_currentPickedRbtNode = g_skyNode; // used later when you do picking
static shared_ptr<SgRbtNode> g_eyeFrameNode = g_skyNode;

// --------- Scene
/*static const Cvec3
  g_light1(2.0, 3.0, 14.0),
  g_light2(-2, -3.0, -5.0);  // define two lights positions in world space */

static RigTForm g_aFrame = RigTForm();



///////////////// END OF G L O B A L S //////////////////////////////////////////////////

static void initGround() {
  int ibLen, vbLen;
  getPlaneVbIbLen(vbLen, ibLen);

  // Temporary storage for cube Geometry
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makePlane(g_groundSize*2, vtx.begin(), idx.begin());
  g_ground.reset(new SimpleIndexedGeometryPNTBX(&vtx[0], &idx[0], vbLen, ibLen));
}

static void initCubes() {
  int ibLen, vbLen;
  getCubeVbIbLen(vbLen, ibLen);

  // Temporary storage for cube Geometry
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeCube(1, vtx.begin(), idx.begin());
  g_cube.reset(new SimpleIndexedGeometryPNTBX(&vtx[0], &idx[0], vbLen, ibLen));
}

static void initSphere() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere Geometry
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(1, 20, 10, vtx.begin(), idx.begin());
  g_sphere.reset(new SimpleIndexedGeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

// takes a projection matrix and send to the the shaders
inline void sendProjectionMatrix(Uniforms& uniforms, const Matrix4& projMatrix) {
  uniforms.put("uProjMatrix", projMatrix);
}

// update g_frustFovY from g_frustMinFov, g_windowWidth, and g_windowHeight
static void updateFrustFovY() {
  if (g_windowWidth >= g_windowHeight)
    g_frustFovY = g_frustMinFov;
  else {
    const double RAD_PER_DEG = 0.5 * CS175_PI/180;
    g_frustFovY = atan2(sin(g_frustMinFov * RAD_PER_DEG) * g_windowHeight / g_windowWidth, cos(g_frustMinFov * RAD_PER_DEG)) / RAD_PER_DEG;
  }
}

static Matrix4 makeProjectionMatrix() {
  return Matrix4::makeProjection(
           g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
           g_frustNear, g_frustFar);
}

//builds auxiliary frame
static void buildAFrame(){

  if (g_eyeFrameNode == g_skyNode && g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0) {
    Quat rot = getPathAccumRbt(g_world, g_currentPickedRbtNode).getRotation();
    g_aFrame = RigTForm(Cvec3(), rot);

  } else {
    Cvec3 trans = getPathAccumRbt(g_world, g_currentPickedRbtNode).getTranslation();
    Quat rot = getPathAccumRbt(g_world, g_eyeFrameNode).getRotation();
    g_aFrame = RigTForm(trans, rot);
  }
}

//creates a RigTForm for an arcball
static RigTForm makeArcballRbt() {

  RigTForm arcballRbt;
  if (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0) {
    arcballRbt = getPathAccumRbt(g_world, g_world);
  } else {
    arcballRbt = getPathAccumRbt(g_world, g_currentPickedRbtNode);
  }

  g_arcballVisible = !g_isPlaying && (g_currentPickedRbtNode != g_eyeFrameNode || (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0));
  return arcballRbt;
}

static void drawStuff(bool picking) {

  Uniforms uniforms;

  // build & send proj. matrix to vshader
  const Matrix4 projmat = makeProjectionMatrix();
  sendProjectionMatrix(uniforms, projmat);

  // choose frame to use as eyeRbt
  RigTForm eyeRbt = getPathAccumRbt(g_world, g_eyeFrameNode);
  
  const RigTForm invEyeRbt = inv(eyeRbt);

  // get world space coordinates of the light
  Cvec3 light1 = getPathAccumRbt(g_world, g_light1Node).getTranslation();
  // transform to eye space, and set to uLight uniform
  uniforms.put("uLight", Cvec3(invEyeRbt * Cvec4(light1, 1)));

  Cvec3 light2 = getPathAccumRbt(g_world, g_light2Node).getTranslation();
  // transform to eye space, and set to uLight uniform
  uniforms.put("uLight2", Cvec3(invEyeRbt * Cvec4(light2, 1)));

   /*
  const Cvec3 eyeLight1 = Cvec3(invEyeRbt * Cvec4(g_light1, 1)); // g_light1 position in eye coordinates
  const Cvec3 eyeLight2 = Cvec3(invEyeRbt * Cvec4(g_light2, 1)); // g_light2 position in eye coordinates
  // safe_glUniform3f(curSS.h_uLight, eyeLight1[0], eyeLight1[1], eyeLight1[2]);
  // safe_glUniform3f(curSS.h_uLight2, eyeLight2[0], eyeLight2[1], eyeLight2[2]);
  uniforms.put("uLight", eyeLight1);
  uniforms.put("uLight2", eyeLight2);
  */

  RigTForm arcballRbt = makeArcballRbt();

  if(!(g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton) || (g_mouseLClickButton && !g_mouseRClickButton && g_spaceDown))){

    // update arcballScale
    g_arcballScale = getScreenToEyeScale((invEyeRbt * arcballRbt).getTranslation()[2], g_frustFovY, g_windowHeight);
  }

  if (!picking) {

    Drawer drawer(invEyeRbt, uniforms);
    g_world->accept(drawer);

    // draw arcball as part of asst3
    if (g_arcballVisible) {
  
      double s = g_arcballScale * g_arcballScreenRadius;

      RigTForm MVMrbt = invEyeRbt * arcballRbt;
      Matrix4 MVM = rigTFormToMatrix(MVMrbt);
      MVM = MVM * Matrix4::makeScale(Cvec3(s, s, s));
  
      Matrix4 NMVM = normalMatrix(MVM);
      sendModelViewNormalMatrix(uniforms, MVM, NMVM);
      
      g_arcballMat->draw(*g_sphere, uniforms);

    }

  } else {//if indeed picking

    Picker picker(invEyeRbt, uniforms);
    g_overridingMaterial = g_pickingMat;
    g_world->accept(picker);
    g_overridingMaterial.reset();

    glFlush();

    g_currentPickedRbtNode = picker.getRbtNodeAtXY(g_mouseClickX, g_mouseClickY);

    if (!g_currentPickedRbtNode || g_currentPickedRbtNode == g_groundNode)
      g_currentPickedRbtNode = g_eyeFrameNode;

    g_arcballVisible = (g_currentPickedRbtNode != g_eyeFrameNode || (g_currentPickedRbtNode == g_skyNode && g_skyModMode == 0));
    g_arcballScale = getScreenToEyeScale((invEyeRbt * arcballRbt).getTranslation()[2], g_frustFovY, g_windowHeight);
  } 
}

//called on GL display call
static void display() {
  // No more glUseProgram

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  drawStuff(false); // no more curSS

  glutSwapBuffers();

  checkGlErrors();
}

static void pick() {

  g_aboutToPick = false;
  // We need to set the clear color to black, for pick rendering.
  // so let's save the clear color
  GLdouble clearColor[4];
  glGetDoublev(GL_COLOR_CLEAR_VALUE, clearColor);

  glClearColor(0, 0, 0, 0);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // No more glUseProgram
  drawStuff(true); // no more curSS

  // Uncomment below and comment out the glutPostRedisplay in mouse(...) call back
  // to see result of the pick rendering pass
  // glutSwapBuffers();

  //Now set back the clear color
  glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);

  checkGlErrors();

  glutPostRedisplay();
}

static void playStop(){

  if(!g_isPlaying && g_keyFrameList.numberOfFrames() >= 4){

    g_isPlaying = true;
    g_keyFrameList.resetCurrentFrame();
    g_previousT = 0;
    g_arcballVisible = false;
    animateTimerCallback(0);

  }else{
    
    makeArcballRbt();    
    g_isPlaying = false;
  }
}

//called on window resize
static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  g_arcballScreenRadius = 0.25 * min(g_windowHeight, g_windowWidth);
  glViewport(0, 0, w, h);
  cerr << "Size of window is now " << w << "x" << h << endl;
  updateFrustFovY();
  glutPostRedisplay();
}

// Create a Quaternion from mouse movement corresponding to arcball
static Quat arcballRotation(const int x, const int y){

  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  RigTForm eyeRbt = getPathAccumRbt(g_world, g_eyeFrameNode);
  RigTForm arcballRbt = makeArcballRbt();

  RigTForm invEyeRbt = inv(eyeRbt);
  Cvec2 arcballScreenCenter = getScreenSpaceCoord((invEyeRbt * arcballRbt).getTranslation(), makeProjectionMatrix(), g_frustNear, g_frustFovY, g_windowWidth, g_windowHeight);

  double x1 = g_mouseClickX - arcballScreenCenter[0];
  double y1 = g_mouseClickY - arcballScreenCenter[1];
  double z1;
  Cvec3 v1;

  double x2 = x - arcballScreenCenter[0];
  double y2 = g_windowHeight - y - 1 - arcballScreenCenter[1];
  double z2;
  Cvec3 v2;

  if (pow(x1, 2) + pow(y1, 2) > pow(g_arcballScreenRadius, 2)) {
    z1 = 0;
    v1 = normalize(Cvec3(x1, y1, z1)) * g_arcballScreenRadius;
  } else {
      z1 = sqrt(pow(g_arcballScreenRadius, 2) - pow(x1, 2) - pow(y1, 2));
      v1 = Cvec3(x1, y1, z1);
  }
  if (pow(x2, 2) + pow(y2, 2) > pow(g_arcballScreenRadius, 2)) {
    z2 = 0;
    v2 = normalize(Cvec3(x2, y2, z2)) * g_arcballScreenRadius;
  } else {
      z2 = sqrt(pow(g_arcballScreenRadius, 2) - pow(x2, 2) - pow(y2, 2));
      v2 = Cvec3(x2, y2, z2);
  }

  v1 = normalize(v1);
  v2 = normalize(v2);

  //Quat rot = Quat(0, v2) * Quat(0, -v1);
  Quat rot = Quat(dot(v1, v2), cross(v1, v2));

  return rot;
}

//called on mouse motion
static void motion(const int x, const int y) {
  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  Matrix4 m;
  RigTForm mRbt;
  

  if (g_mouseLClickButton && !g_mouseRClickButton && !g_spaceDown) { // left button down?

    if (g_arcballVisible){
      mRbt = RigTForm(arcballRotation(x, y));
    } else {
      mRbt = RigTForm(Quat::makeXRotation(-dy) * Quat::makeYRotation(dx));
    }
  }
  else if (g_mouseRClickButton && !g_mouseLClickButton) { // right button down?

    if (g_arcballVisible) {
      mRbt = RigTForm(Cvec3(dx, dy, 0) * g_arcballScale);
    } else {
      mRbt = RigTForm(Cvec3(dx, dy, 0) * 0.01);
    }
    
  }
  else if (g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton) || (g_mouseLClickButton && !g_mouseRClickButton && g_spaceDown)) {  // middle or (left and right, or left + space) button down?

    if (g_arcballVisible) {
      mRbt = RigTForm(Cvec3(0, 0, -dy) * g_arcballScale);
    } else {
      mRbt = RigTForm(Cvec3(0, 0, -dy) * 0.01);
    }
    
  }

  buildAFrame();

  if (g_mouseClickDown) {
    
    RigTForm curRbt = g_currentPickedRbtNode->getRbt();

    if(g_currentPickedRbtNode == g_skyNode) {

      if (g_skyModMode == 0) {
          Quat rot = mRbt.getRotation();
          mRbt = RigTForm(-(mRbt.getTranslation()), Quat(rot[0], -(rot[1]), -(rot[2]), -(rot[3])));
          g_skyNode->setRbt(g_aFrame * mRbt * inv(g_aFrame) * curRbt);
      } else {
          Quat rot = mRbt.getRotation();
          mRbt = RigTForm(mRbt.getTranslation(), Quat(rot[0], -(rot[1]), -(rot[2]), -(rot[3])));
          g_skyNode->setRbt(g_aFrame * mRbt * inv(g_aFrame) * curRbt);
      }
    }else {
      
      //g_arcballRbt = g_aFrame * mRbt * inv(g_aFrame) * g_arcballRbt;
      RigTForm newAFrame = inv(getPathAccumRbt(g_world, g_currentPickedRbtNode, 1)) * g_aFrame;
      g_currentPickedRbtNode->setRbt(newAFrame * mRbt * inv(newAFrame) * curRbt);
    } 
    glutPostRedisplay(); // we always redraw if we changed the scene
  }

  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;
}

//called on mouse button state change
static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;

  ///

  if(g_mouseLClickButton){

    if(g_aboutToPick){
      pick();
    }
  }
}

static void keyboardUp(const unsigned char key, const int x, const int y) {
  switch (key) {
  case ' ':
    g_spaceDown = false;
    break;
  }
  glutPostRedisplay();
}

static void keyboard(const unsigned char key, const int x, const int y) {
  switch (key) {
  case 27:
    exit(0);                                  // ESC
  case 'h':
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "s\t\tsave screenshot\n"
    << "f\t\tToggle flat shading on/off.\n"
    << "o\t\tCycle object to edit\n"
    << "v\t\tCycle view\n"
    << "drag left mouse to rotate\n" << endl;
    break;
  case 's':
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    break;
  case 'p':
    g_aboutToPick = true;
    break;
  case 'v':    
    if(g_eyeFrameNode == g_skyNode){

      g_eyeFrameNode = g_robot1Node;
      g_currentPickedRbtNode = g_robot1Node;
      makeArcballRbt();
      cout << "current view: robot 1" << endl;
    }
    else if (g_eyeFrameNode == g_robot1Node) {
      g_eyeFrameNode = g_robot2Node;
      g_currentPickedRbtNode = g_robot2Node;
      makeArcballRbt();
      cout << "current view: robot 2" << endl; 
    }
    else if (g_eyeFrameNode == g_robot2Node) {
      g_eyeFrameNode = g_skyNode;
      makeArcballRbt();
      cout << "current view: sky camera" << endl; 
    }
    //resetArcball();
    break;
  case 'm':
      g_skyModMode = (g_skyModMode + 1) % 2;
      switch(g_skyModMode){
        case 0:
          cout << "sky-world" << endl;
          break;
        case 1:
          cout << "sky-sky" << endl;
          break;
      }
    //resetArcball();
    break;

  //animations
  case 'c':
    g_keyFrameList.pullToFrame();
    break;

  case 'u':
    g_keyFrameList.pushFrame();
    break;

  case '<':
    g_keyFrameList.prevFrame();
    break;

  case '>':
    g_keyFrameList.nextFrame();
    break;

  case 'd':
    g_keyFrameList.deleteFrame();
    break;

  case 'n':
    g_keyFrameList.newFrame();
    break;

  case 'w':
    g_keyFrameList.saveFrames();
    break;

  case 'i':
    g_keyFrameList.loadFrames();
    break;

  case 'y':
    playStop();
    break;

  case '+':
    speedAnimationUp();
    break;
  case '-':
    slowAnimationDown();
    break;

  case ' ':
    g_spaceDown = true;
    break;
  }
  glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
#ifdef __MAC__
  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE|GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH); // core profile flag is required for GL 3.2 on Mac
#else
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
#endif
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("Assignment 2");                       // title the window

  glutIgnoreKeyRepeat(true);                              // avoids repeated keyboard calls when holding space to emulate middle mouse

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutMotionFunc(motion);                                 // mouse movement callback
  glutMouseFunc(mouse);                                   // mouse click callback
  glutKeyboardFunc(keyboard);
  glutKeyboardUpFunc(keyboardUp);
}

static void initGLState() {
  glClearColor(128./255., 200./255., 255./255., 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initMaterials() {
  // Create some prototype materials
  Material diffuse("./shaders/basic-gl3.vshader", "./shaders/diffuse-gl3.fshader");
  Material solid("./shaders/basic-gl3.vshader", "./shaders/solid-gl3.fshader");

  // copy diffuse prototype and set red color
  g_redDiffuseMat.reset(new Material(diffuse));
  g_redDiffuseMat->getUniforms().put("uColor", Cvec3f(1, 0, 0));

  // copy diffuse prototype and set blue color
  g_blueDiffuseMat.reset(new Material(diffuse));
  g_blueDiffuseMat->getUniforms().put("uColor", Cvec3f(0, 0, 1));

  // normal mapping material
  g_bumpFloorMat.reset(new Material("./shaders/normal-gl3.vshader", "./shaders/normal-gl3.fshader"));
  g_bumpFloorMat->getUniforms().put("uTexColor", shared_ptr<ImageTexture>(new ImageTexture("Fieldstone.ppm", true)));
  g_bumpFloorMat->getUniforms().put("uTexNormal", shared_ptr<ImageTexture>(new ImageTexture("FieldstoneNormal.ppm", false)));

  // copy solid prototype, and set to wireframed rendering
  g_arcballMat.reset(new Material(solid));
  g_arcballMat->getUniforms().put("uColor", Cvec3f(0.27f, 0.82f, 0.35f));
  g_arcballMat->getRenderStates().polygonMode(GL_FRONT_AND_BACK, GL_LINE);

  // copy solid prototype, and set to color white
  g_lightMat.reset(new Material(solid));
  g_lightMat->getUniforms().put("uColor", Cvec3f(1, 1, 1));

  // pick shader
  g_pickingMat.reset(new Material("./shaders/basic-gl3.vshader", "./shaders/pick-gl3.fshader"));
};

static void initGeometry() {
  initGround();
  initCubes();
  initSphere();
}

static void constructRobot(shared_ptr<SgTransformNode> base, shared_ptr<Material> material) {

  const double ARM_LEN = 0.7,
               ARM_THICK = 0.25,
               TORSO_LEN = 1.5,
               TORSO_THICK = 0.25,
               TORSO_WIDTH = 1,
               HEAD_RADIUS = 0.4;
  const int NUM_JOINTS = 10,
            NUM_SHAPES = 10;

  struct JointDesc {
    int parent;
    float x, y, z;
  };

  JointDesc jointDesc[NUM_JOINTS] = {
    {-1, 0, 0, 0}, // torso
    {0, +TORSO_WIDTH/2, TORSO_LEN/2, 0}, // upper right arm
    {1, +ARM_LEN, 0, 0}, // lower right arm
    {0, -TORSO_WIDTH/2, TORSO_LEN/2, 0}, //upper left arm
    {3, -ARM_LEN, 0, 0}, //lower left arm

    {0, +TORSO_WIDTH*0.4, -TORSO_LEN/2, 0}, //upper right leg
    {5, 0, -ARM_LEN, 0}, //lower right leg
    {0, -TORSO_WIDTH*0.4, -TORSO_LEN/2, 0}, // upper left leg
    {7, 0, -ARM_LEN, 0}, //lower left leg

    {0, 0, +TORSO_LEN/2, 0}, //head
  };

  struct ShapeDesc {
    int parentJointId;
    float x, y, z, sx, sy, sz;
    shared_ptr<Geometry> geometry;
  };

  ShapeDesc shapeDesc[NUM_SHAPES] = {
    {0, 0,         0, 0, TORSO_WIDTH, TORSO_LEN, TORSO_THICK, g_cube}, // torso
    {1, +ARM_LEN/2, 0, 0, ARM_LEN * 0.55, ARM_THICK * 0.6, ARM_THICK * 0.6, g_sphere}, // upper right arm
    {2, +ARM_LEN/2, 0, 0, ARM_LEN, ARM_THICK, ARM_THICK, g_cube}, // lower right arm
    {3, -ARM_LEN/2, 0, 0, ARM_LEN * 0.55, ARM_THICK * 0.6, ARM_THICK * 0.6, g_sphere}, //upper left arm
    {4, -ARM_LEN/2, 0, 0, ARM_LEN, ARM_THICK, ARM_THICK, g_cube}, //lower left arm

    {5, 0, -ARM_LEN/2, 0, ARM_THICK * 0.6, ARM_LEN * 0.55, ARM_THICK * 0.6, g_sphere}, //upper right leg
    {6, 0, -ARM_LEN/2, 0, ARM_THICK, ARM_LEN, ARM_THICK, g_cube}, //lower right leg
    {7, 0, -ARM_LEN/2, 0, ARM_THICK * 0.6, ARM_LEN * 0.55, ARM_THICK * 0.6, g_sphere}, //upper left leg
    {8, 0, -ARM_LEN/2, 0, ARM_THICK, ARM_LEN, ARM_THICK, g_cube}, //lower left leg

    {9, 0, HEAD_RADIUS, 0, HEAD_RADIUS, HEAD_RADIUS, HEAD_RADIUS, g_sphere}, //head
  };

  shared_ptr<SgTransformNode> jointNodes[NUM_JOINTS];

  for (int i = 0; i < NUM_JOINTS; ++i) {
    if (jointDesc[i].parent == -1)
      jointNodes[i] = base;
    else {
      jointNodes[i].reset(new SgRbtNode(RigTForm(Cvec3(jointDesc[i].x, jointDesc[i].y, jointDesc[i].z))));
      jointNodes[jointDesc[i].parent]->addChild(jointNodes[i]);
    }
  }
    // The new MyShapeNode takes in a material as opposed to color
  for (int i = 0; i < NUM_SHAPES; ++i) {
    shared_ptr<SgGeometryShapeNode> shape(
      new MyShapeNode(shapeDesc[i].geometry,
                      material, // USE MATERIAL as opposed to color
                      Cvec3(shapeDesc[i].x, shapeDesc[i].y, shapeDesc[i].z),
                      Cvec3(0, 0, 0),
                      Cvec3(shapeDesc[i].sx, shapeDesc[i].sy, shapeDesc[i].sz)));
    jointNodes[shapeDesc[i].parentJointId]->addChild(shape);
  }
}

static void initScene() {
  g_world.reset(new SgRootNode());

  g_skyNode.reset(new SgRbtNode(RigTForm(Cvec3(0.0, 0.25, 4.0))));

  g_currentPickedRbtNode = g_skyNode;
  g_eyeFrameNode = g_skyNode;

  g_groundNode.reset(new SgRbtNode());
  g_groundNode->addChild(shared_ptr<MyShapeNode>(
                          new MyShapeNode(g_ground, g_bumpFloorMat, Cvec3(0, g_groundY, 0))));

  g_light1Node.reset(new SgRbtNode(RigTForm(Cvec3(2.0, 3.0, 14.0))));
  g_light1Node->addChild(shared_ptr<MyShapeNode>(
                          new MyShapeNode(g_sphere, g_lightMat, Cvec3())));

  g_light2Node.reset(new SgRbtNode(RigTForm(Cvec3(-2, -3.0, -5.0))));
  g_light2Node->addChild(shared_ptr<MyShapeNode>(
                          new MyShapeNode(g_sphere, g_lightMat, Cvec3())));



  g_robot1Node.reset(new SgRbtNode(RigTForm(Cvec3(-2, 1, 0))));
  g_robot2Node.reset(new SgRbtNode(RigTForm(Cvec3(2, 1, 0))));

  constructRobot(g_robot1Node, g_redDiffuseMat); // a Red robot
  constructRobot(g_robot2Node, g_blueDiffuseMat); // a Blue robot

  g_world->addChild(g_skyNode);
  g_world->addChild(g_groundNode);
  g_world->addChild(g_robot1Node);
  g_world->addChild(g_robot2Node);
  g_world->addChild(g_light1Node);
  g_world->addChild(g_light2Node);

  g_keyFrameList.init(g_world);
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

  // on Mac, we shouldn't use GLEW.

#ifndef __MAC__
    glewInit(); // load the OpenGL extensions
#endif

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.5") << endl;

#ifndef __MAC__
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");
#endif

    initGLState();
    initMaterials();
    initGeometry();
    initScene();

    glutMainLoop();
    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
