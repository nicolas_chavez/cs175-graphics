#ifndef SGUTILS_H
#define SGUTILS_H

#include <vector>
#include <list>

#include <fstream>
#include <string>
#include <sstream>

#include "scenegraph.h"

using namespace std;
using namespace std::tr1;

class KeyFrameList;

struct RbtNodesScanner : public SgNodeVisitor {
  typedef std::vector<std::tr1::shared_ptr<SgRbtNode> > SgRbtNodes;

  SgRbtNodes& nodes_;

  RbtNodesScanner(SgRbtNodes& nodes) : nodes_(nodes) {}

  virtual bool visit(SgTransformNode& node) {
    using namespace std;
    using namespace tr1;
    shared_ptr<SgRbtNode> rbtPtr = dynamic_pointer_cast<SgRbtNode>(node.shared_from_this());
    if (rbtPtr)
      nodes_.push_back(rbtPtr);
    return true;
  }
};

inline void dumpSgRbtNodes(shared_ptr<SgNode> root, vector<shared_ptr<SgRbtNode> >& rbtNodes) {
  RbtNodesScanner scanner(rbtNodes);
  root->accept(scanner);
}

class Frame {

  vector<RigTForm> rbts_;

public:

  Frame() {
  }

  const vector<RigTForm>& getRbts() const {

    return rbts_;
  }

  void setRbts(const vector<RigTForm>& rbts){

    rbts_ = rbts;
  }

  friend ostream& operator<<(ostream& out, Frame& f) {

    vector<RigTForm> rbts = f.getRbts();

    for(vector<RigTForm>::iterator iter = rbts.begin(), end = rbts.end(); iter != end; ++iter){

      vector<RigTForm>::iterator iter2 = iter;
      iter2++;

      if(iter2 == end)
        out << *iter;
      else
        out << *iter << "|";
    }

    return out;
  }
};

class KeyFrameList {

  list<Frame> frames_;
  vector<shared_ptr<SgRbtNode> > nodes_;
  list<Frame>::iterator currentFrame_;
  
  void printCurrentFrame(){

    cout << distance(frames_.begin(), currentFrame_) << endl;
  }

  vector<string> &split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
  }


  vector<string> split(const string &s, char delim) {
      vector<string> elems;
      split(s, delim, elems);
      return elems;
  }

public:

  KeyFrameList() {

    currentFrame_ = frames_.end();
  }

  void init(const shared_ptr<SgRootNode>& rootNode){

    assert(nodes_.size() == 0);

    dumpSgRbtNodes(rootNode, nodes_);
  }

  //sets currentFrame to 0
  void resetCurrentFrame(){

    currentFrame_ = frames_.begin();
  }

  //pushes currentFrame up without pushing frame to scene
  //returns true if last frame
  bool nextFrameSoft(){

    if(currentFrame_ != frames_.end())
      ++currentFrame_;

    return currentFrame_ == frames_.end();
  }

  //'u'
  void pushFrame(){

    pushRbts(currentFrame_->getRbts());
  }

  void pushRbts(const vector<RigTForm>& rbts){

    if(currentFrame_ != frames_.end()){

      //set rbts into nodes
      for(int i = 0; i < rbts.size() && i < nodes_.size(); i++){

        nodes_[i]->setRbt(rbts[i]);
      }
    }

    //printCurrentFrame();
  }

  //'c'
  void pullToFrame(){

    if(currentFrame_ != frames_.end()){

      vector<RigTForm> rbts;

      for(vector<shared_ptr<SgRbtNode> >::iterator i = nodes_.begin(), end = nodes_.end(); i != end; ++i){

        //add rbt data from each node for frame
        RigTForm rbt = (*i)->getRbt();
        rbts.push_back(rbt);
      }
      currentFrame_->setRbts(rbts);

      printCurrentFrame();

    }else //new frame if no frames
      newFrame();
  }

  //'>'
  void nextFrame(){
    list<Frame>::iterator iter2 = currentFrame_;
    iter2++;
    if(iter2 != frames_.end()){
    
      currentFrame_ = iter2;
    }

    pushFrame();
    printCurrentFrame();
  }

  //'<'
  void prevFrame(){

    if(currentFrame_ != frames_.begin() && currentFrame_ != frames_.end()){
     
      currentFrame_--;
    }

    pushFrame();
    printCurrentFrame();
  }

  //'d'
  void deleteFrame(){

    if(currentFrame_ != frames_.end()){

      list<Frame>::iterator nextCurrent = currentFrame_;

      if(currentFrame_ != frames_.begin())
        --nextCurrent;
      else
        ++nextCurrent;

      frames_.erase(currentFrame_);

      currentFrame_ = nextCurrent;
    }

    pushFrame();
    printCurrentFrame();
  }

  //'n'
  void newFrame(){

    Frame newF;
    vector<RigTForm> rbts;

    for(vector<shared_ptr<SgRbtNode> >::iterator i = nodes_.begin(), end = nodes_.end(); i != end; ++i){

      //add rbt data from each node for frame
      RigTForm rbt = (*i)->getRbt();
      rbts.push_back(rbt);
    }

    newF.setRbts(rbts);

    //insert frame after current, if undefined, should be first
    list<Frame>::iterator toInsertBefore = currentFrame_;
    if(toInsertBefore != frames_.end())
      ++toInsertBefore;

    currentFrame_ = frames_.insert(toInsertBefore, newF);

    printCurrentFrame();
  }

  inline bool fileExists (const std::string& name) {
    ifstream f(name.c_str());
    if (f.good()) {
        f.close();
        return true;
    } else {
        f.close();
        return false;
    } 
  }

  //saves in format:
  //  (x,y,z)(a,b,c,d)|(x,y,z)(a,b,c,d)|...
  // Each line is one frame
  // Each line is split by '|', which has (x,y,z)(a,b,c,d)
  // Each (x,y,z)(a,b,c,d) is an rbt with the translation followed by the quaternion

  //'i'
  void loadFrames(){


    if(!fileExists("out.kfr")){
      cout << "File does not exist!" << endl;
      return;
    }

    ifstream inFile;
    inFile.open("out.kfr", ios::in);

    string line;
    frames_.clear();
    currentFrame_ = frames_.end();

    while(!inFile.eof()){

      getline(inFile, line);
      
      vector<RigTForm> rbts;
      vector<string> rbtStrings = split(line, '|');

      if(rbtStrings.size() == 0)
        continue;

      for(vector<string>::iterator iter = rbtStrings.begin(), end = rbtStrings.end(); iter != end; ++iter){

        RigTForm rtf(iter->c_str());
        rbts.push_back(rtf);
      }
      Frame f;
      f.setRbts(rbts);

      frames_.push_back(f);
    }

    inFile.close();

    currentFrame_ = frames_.begin();

    cout << "Read file!" << endl;
  }

  //'w'
  void saveFrames(){

    ofstream outFile;

    outFile.open("out.kfr", ios::out | ios::trunc);

    for(list<Frame>::iterator iter = frames_.begin(), end = frames_.end(); iter != end; ++iter){

      outFile << *iter << endl;
    }

    outFile.close();

    cout << "Wrote to file!" << endl;

  }

  int numberOfFrames(){

    return frames_.size();
  }

  const Frame * getFrameBack(const int pHowFar){

    const Frame *ret = NULL;
    int howFar = pHowFar;

    if(currentFrame_ != frames_.end()){

      list<Frame>::iterator iter2 = currentFrame_;
      
      while(howFar > 1){

        //bound at beginning frame
        if(iter2 != frames_.begin())
          --iter2;
        --howFar;
      }

      cout << "back " << pHowFar << ": " << distance(frames_.begin(), iter2) << endl;
      const Frame& f = *iter2;
      ret = &f;
    
    }else
      cout << "back " << pHowFar << ": NULL" << endl;
    return ret;
  }

  const Frame * getFrameAhead(const int pHowFar){

    const Frame *ret = NULL;
    int howFar = pHowFar;

    list<Frame>::iterator iter2 = currentFrame_;
    ++iter2;

    while(howFar > 1){
        ++iter2;
        --howFar;
    }

    if(iter2 != frames_.end()){

      const Frame& f = *iter2;
      ret = &f;
      cout << "ahead " << pHowFar << ": " << distance(frames_.begin(), iter2) << endl;

    }else
      cout << "ahead " << pHowFar << ": NULL" << endl;

    return ret;
  }
};

#endif