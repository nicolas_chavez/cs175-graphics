#ifndef ANIMATION_H 
#define ANIMATION_H

#include <cassert>
#include <cmath>

#include "cvec.h"
#include "matrix4.h"

// ------- Animation
static KeyFrameList g_keyFrameList;
static bool g_isPlaying = false;

static int g_msBetweenKeyFrames = 2000; // 2 seconds between keyframes
static int g_animateFramesPerSecond = 60; // frames to render per second during animation playback
static float g_previousT = 0;


static Cvec3 lerp(const Cvec3& c1, const Cvec3& c2, double alpha) {
  assert(alpha >= 0 && alpha <= 1);
  if (c1 == c2) {
    return c1;
  }
  return (c1 * (1 - alpha)) + (c2 * alpha);
}

static Quat slerp(const Quat& q1, const Quat& q2, double alpha) {
  assert(alpha >= 0 && alpha <= 1);
  if (q1 == q2) {
    return q1;
  }

  Quat q = q2 * inv(q1);
  bool cn = (q[0] < 0);
  if(cn) {
    q = Quat(-q[0], -q[1], -q[2], -q[3]);
  }
  Quat qa = pow(q, alpha);
  return qa * q1;
}

static Quat quatCalculateD(const Quat& c1, const Quat& c2, const Quat& c3) {

  Quat q = c3 * inv(c1);
  bool cn = (q[0] < 0);
  if(cn) {
    q = Quat(-q[0], -q[1], -q[2], -q[3]);
  } 
  return pow(q, (1.0/6.0)) * c2;

}

static Quat quatCalculateE(const Quat& c1, const Quat& c2, const Quat& c3) {

  Quat q = c3 * inv(c1);
  bool cn = (q[0] < 0);
  if(cn) {
    q = Quat(-q[0], -q[1], -q[2], -q[3]);
  }
  
  return pow(q, ((-1.0)/6.0)) * c2;

}

static Cvec3 cvecCalculateD(const Cvec3& c1, const Cvec3& c2, const Cvec3& c3) {
  Cvec3 c = ((c3 - c1) * (1.0/6.0));
  return c + c2;
}

static Cvec3 cvecCalculateE(const Cvec3& c1, const Cvec3& c2, const Cvec3& c3) {
  Cvec3 c = (c3 - c1) * ((-1.0)/6.0);
  return c + c2;  
}

static Cvec3 catmullRomCvec (const Cvec3& c1, const Cvec3& c2, const Cvec3& c3, const Cvec3& c4, double alpha){
  Cvec3 d = cvecCalculateD(c1, c2, c3);
  Cvec3 e = cvecCalculateE(c2, c3, c4);

  Cvec3 p = lerp(c2, d, alpha);
  Cvec3 q = lerp(d, e, alpha);
  Cvec3 r = lerp(e, c3, alpha);

  Cvec3 a = lerp(p, q, alpha);
  Cvec3 b = lerp(q, r, alpha);

  return lerp(a, b, alpha);

}

static Quat catmullRomQuat (const Quat& c1, const Quat& c2, const Quat& c3, const Quat& c4, double alpha){
  Quat d = quatCalculateD(c1, c2, c3);
  Quat e = quatCalculateE(c2, c3, c4);

  Quat p = slerp(c2, d, alpha);
  Quat q = slerp(d, e, alpha);
  Quat r = slerp(e, c3, alpha);

  Quat a = slerp(p, q, alpha);
  Quat b = slerp(q, r, alpha);

  return slerp(a, b, alpha);
}

//a in [0,1]
// creates a new frame from back to ahead, with a
static void interpolateRbts(const vector<RigTForm>& back2, const vector<RigTForm>& back1, const vector<RigTForm>& ahead1, 
                            const vector<RigTForm>&ahead2, vector<RigTForm>&interRbts, float a){

  //cout << "alpha: " << a << endl;

  for(int i = 0; i < back2.size() && i < back1.size() && i < ahead1.size() && i < ahead2.size(); ++i){

    RigTForm back2Rbt = back2[i];
    RigTForm back1Rbt = back1[i];
    RigTForm ahead1Rbt = ahead1[i];
    RigTForm ahead2Rbt = ahead2[i];

    Cvec3 interTrans = catmullRomCvec(back2Rbt.getTranslation(), back1Rbt.getTranslation(), ahead1Rbt.getTranslation(), ahead2Rbt.getTranslation(), a);
    Quat interRot = catmullRomQuat(back2Rbt.getRotation(), back1Rbt.getRotation(), ahead1Rbt.getRotation(), ahead2Rbt.getRotation(), a);

    //add to frame
    RigTForm interRbt = RigTForm(interTrans, interRot);

    interRbts.push_back(interRbt); 
  }
}

// Given t in the range [0, n], perform interpolation and draw the scene
// for the particular t. Returns true if we are at the end of the animation
// sequence, or false otherwise.
static bool interpolateAndDisplay(float t) {

  //oldT = 3.9, newT = 4.1
  //To move the iterator up so the frame getting works correctly
  bool finished = false;
  if(floor(t) != floor(g_previousT))
    finished = g_keyFrameList.nextFrameSoft();

  if(t >= g_keyFrameList.numberOfFrames() - 2)
    finished = true;

  if(finished)
    return true;

  cout << "t: " << t << endl;

  const Frame *back2 = g_keyFrameList.getFrameBack(2);
  const Frame *back1 = g_keyFrameList.getFrameBack(0);
  const Frame *ahead1 = g_keyFrameList.getFrameAhead(0);
  const Frame *ahead2 = g_keyFrameList.getFrameAhead(2);

  cout << endl << endl;
  //check for NULL
  if(back2 != NULL && back1 != NULL && ahead1 != NULL && ahead2 != NULL){

    //interpolate the two frames
    vector<RigTForm> back2Frame = back2->getRbts();
    vector<RigTForm> back1Frame = back1->getRbts();
    vector<RigTForm> ahead1Frame = ahead1->getRbts();
    vector<RigTForm> ahead2Frame = ahead2->getRbts();

    vector<RigTForm> interFrame;
    interpolateRbts(back2Frame, back1Frame, ahead1Frame, ahead2Frame, interFrame, t - floor(t));

    //push to scene graph
    g_keyFrameList.pushRbts(interFrame);

    glutPostRedisplay();
  }
  g_previousT = t;
  return false;
}

// Interpret "ms" as milliseconds into the animation
static void animateTimerCallback(int ms) {

  float t = (float)ms/(float)g_msBetweenKeyFrames;
  t *= g_keyFrameList.numberOfFrames();

  //cout << t << endl;

  bool endReached = interpolateAndDisplay(t + 1);

  if (!endReached && g_isPlaying)

    glutTimerFunc(1000/g_animateFramesPerSecond,
      animateTimerCallback,
      ms + 1000/g_animateFramesPerSecond);

  else {
    
    g_isPlaying = false;
  }
}

static void speedAnimationUp(){

  //cap at 120!
  if(g_msBetweenKeyFrames <= 500)
    g_msBetweenKeyFrames = 500;
  else
    g_msBetweenKeyFrames -= 100;
}

static void slowAnimationDown(){

  if(g_msBetweenKeyFrames >= 4000)
    g_msBetweenKeyFrames = 4000;
  else
    g_msBetweenKeyFrames += 100;
}


#endif