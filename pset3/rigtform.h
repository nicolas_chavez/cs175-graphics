#ifndef RIGTFORM_H
#define RIGTFORM_H

#include <iostream>
#include <cassert>

#include "matrix4.h"
#include "quat.h"

class RigTForm {
  Cvec3 t_; // translation component
  Quat r_;  // rotation component represented as a quaternion

public:
  RigTForm() : t_(0) {
    assert(norm2(Quat(1,0,0,0) - r_) < CS175_EPS2);
  }

  RigTForm(const Cvec3& t, const Quat& r) {
    
	  t_ = t, r_ = r;
  }

  explicit RigTForm(const Cvec3& t) {
    
	  t_ = t;
  }

  explicit RigTForm(const Quat& r) {
    
	  r_ = r;
  }

  RigTForm(const RigTForm& rtf){

    t_ = Cvec3(rtf.getTranslation()[0], rtf.getTranslation()[1], rtf.getTranslation()[2]);
    r_ = Quat(rtf.getRotation()[0], rtf.getRotation()[1], rtf.getRotation()[2], rtf.getRotation()[3]);
  }

  Cvec3 getTranslation() const {
    return t_;
  }

  Quat getRotation() const {
    return r_;
  }

  RigTForm& setTranslation(const Cvec3& t) {
    t_ = t;
    return *this;
  }

  RigTForm& setRotation(const Quat& r) {
    r_ = r;
    return *this;
  }

  Cvec4 operator * (const Cvec4& a) const {
    
	  return r_ * a + Cvec4(t_[0], t_[1], t_[2], 1);
  }

  RigTForm operator * (const RigTForm& a) const {
	  
	  Cvec4 t2 = Cvec4(a.getTranslation()[0], a.getTranslation()[1], a.getTranslation()[2], 1);
	  Cvec4 r1t2 = r_ * t2;
	  return RigTForm(t_ + Cvec3(r1t2[0], r1t2[1], r1t2[2]), r_ * a.getRotation());
  }
};

inline RigTForm inv(const RigTForm& tform) {
  
	Quat rInv = inv(tform.getRotation());
	Cvec4 newT = rInv * Cvec4(tform.getTranslation()[0], tform.getTranslation()[1], tform.getTranslation()[2], 1);
	newT = -newT;

	return RigTForm(Cvec3(newT[0], newT[1], newT[2]), rInv);
}

inline RigTForm transFact(const RigTForm& tform) {
  return RigTForm(tform.getTranslation());
}

inline RigTForm linFact(const RigTForm& tform) {
  return RigTForm(tform.getRotation());
}

inline Matrix4 rigTFormToMatrix(const RigTForm& tform) {
  
	Matrix4 T = Matrix4::makeTranslation(tform.getTranslation());
	Matrix4 R = quatToMatrix(tform.getRotation());
	return T * R;
}

inline RigTForm makeHybrid(const RigTForm& keepAxes, const RigTForm& keepTrans){
	
	return RigTForm(keepTrans.getTranslation(), keepAxes.getRotation());
}

#endif
